﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Constants;

public class PieceMove : MonoBehaviour
{
    private const float MOVE_SPEED = 0.2f;

    private const float MOVE_HEIGHT = 3f;

    private BoardManager m_BoardManager;

    private DisplayManager m_DisplayManager;

    private StateManager m_StateManager;

    private int[,] m_StageInfo;

    private GameObject m_MovePiece;

    private Vector3 m_GoalPos;

    private void Awake()
    {
        m_BoardManager = GetComponent<BoardManager>();
        m_DisplayManager = GetComponent<DisplayManager>();
        m_StateManager = GetComponent<StateManager>();
    }

    /// <summary>
    /// 駒の移動処理(盤面)
    /// </summary>
    /// <param name="l_TouchIndex">タッチされたマスのインデックス</param>
    /// <param name="l_PieceObj">移動する駒のオブジェクト</param>
    /// <param name="l_PieceIndex">移動前の駒のインデックス</param>
    public void Move(Grid l_TouchIndex, GameObject l_PieceObj, Grid l_PieceIndex)
    {
        m_StageInfo = m_BoardManager.ReturnStageInfo();

        int l_UpdateBit = m_StageInfo[l_PieceIndex.X, l_PieceIndex.Y] & (~Bit.PIECE);
        m_BoardManager.GridUpdate(l_PieceIndex, l_UpdateBit);

        l_UpdateBit = m_StageInfo[l_TouchIndex.X, l_TouchIndex.Y] | Bit.PIECE;
        m_BoardManager.GridUpdate(l_TouchIndex, l_UpdateBit);
    }

    /// <summary>
    /// 描画処理準備
    /// </summary>
    /// <param name="l_TouchIndex">タッチされたマスのインデックス</param>
    /// <param name="l_PieceObj">移動する駒のオブジェクト</param>
    public void MoveSetup(Grid l_TouchIndex, GameObject l_PieceObj)
    {
        m_MovePiece = l_PieceObj;
        GameObject l_GridObj = m_DisplayManager.ReturnStageObj(l_TouchIndex);
        m_GoalPos = new Vector3(l_GridObj.transform.position.x, m_MovePiece.transform.position.y, l_GridObj.transform.position.z);

        //移動先に半透明の駒出現
        m_DisplayManager.SpawnPieceShadow(m_GoalPos);
    }

    /// <summary>
    /// 駒の移動処理(描画)
    /// </summary>
    public IEnumerator PieceObjMove(UnityAction<bool> l_CallBack)
    {
        m_DisplayManager.RemovePieceShadow();
        Vector3 l_StartPos = m_MovePiece.transform.position;
        Vector3 l_CenterPos = new Vector3((m_MovePiece.transform.position.x + m_GoalPos.x) * 0.5f, m_MovePiece.transform.position.y , (m_MovePiece.transform.position.z + m_GoalPos.z) * 0.5f); 
        l_CenterPos.y += MOVE_HEIGHT;
        float l_Distance = Vector3.Distance(m_MovePiece.transform.position, m_GoalPos);
        float l_CurrentRate = 0;

        while (l_CurrentRate < 1)
        {
            l_CurrentRate += Time.deltaTime / MOVE_SPEED;
            m_MovePiece.transform.position = Vector3.Lerp(l_StartPos, l_CenterPos, l_CurrentRate);
            yield return null;
        }

        l_CurrentRate = 0;

        while (l_CurrentRate < 1)
        {
            l_CurrentRate += Time.deltaTime/MOVE_SPEED;
            m_MovePiece.transform.position = Vector3.Lerp(l_CenterPos, m_GoalPos, l_CurrentRate);
            yield return null;
        }
        l_CallBack(true);
        yield break;
    }
}
