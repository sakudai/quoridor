﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalManager : MonoBehaviour
{
    private BoardManager m_BoardManager;

    private int[,] m_StageInfo;

    private int m_WinNum;

    [SerializeField]
    private Text m_Text;

    /// <summary>
    /// Key値:駒の番号、Value値:ゴールの座標
    /// </summary>
    private Dictionary<int, Grid> m_GoalIndexList = new Dictionary<int, Grid>();

    private void Awake()
    {
        m_BoardManager = GetComponent<BoardManager>();
    }

    /// <summary>
    /// 駒初期配置
    /// </summary>
    /// <param name="l_PieceCount">駒の総数</param>
    public void AddListGoalIndex(int l_PieceCount)
    {
        m_StageInfo = m_BoardManager.ReturnStageInfo();
        int l_CenterValue = m_StageInfo.GetLength(0) / 2;
        Grid l_Index = new Grid();

        l_Index.X = 0;
        l_Index.Y = -1;
        m_GoalIndexList.Add(0, l_Index);

        l_Index.X = m_StageInfo.GetLength(1) - 1;
        l_Index.Y = -1;
        m_GoalIndexList.Add(1, l_Index);

        if (l_PieceCount <= 2) { return; }

        l_Index.X = -1;
        l_Index.Y = m_StageInfo.GetLength(0) - 1;
        m_GoalIndexList.Add(2, l_Index);

        l_Index.X = -1;
        l_Index.Y = 0;
        m_GoalIndexList.Add(3, l_Index);
    }

    /// <summary>
    /// ゴールチェック
    /// </summary>
    /// <param name="l_PieceNum">チェックする駒の番号</param>
    /// <param name="l_Index">駒の現在の座標</param>
    /// <returns></returns>
    public bool GoalCheck(int l_PieceNum, Grid l_Index)
    {
        if(m_GoalIndexList[l_PieceNum].X == l_Index.X || m_GoalIndexList[l_PieceNum].Y == l_Index.Y)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// ゴールのインデックスを返す
    /// </summary>
    /// <param name="l_PieceNum">駒の番号</param>
    /// <returns></returns>
    public Grid ReturnGoalIndex(int l_PieceNum)
    {
        return m_GoalIndexList[l_PieceNum];
    }

    /// <summary>
    /// 勝利したプレイヤーを表示
    /// </summary>
    /// <param name="l_WinPieceNum">勝利した駒の番号</param>
    public void DisplayWinNum(int l_WinPieceNum)
    {
        m_WinNum = l_WinPieceNum;
        m_Text.enabled = true;
        m_Text.text ="PLAYER "+ (m_WinNum+1) + " WIN";
    }
}
