﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateStage : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Panel;

    private float m_AdjustmentValue = 1.5f;

    private DisplayManager m_DisplayManager;

    private PieceManager m_PieceManager;

    private void Awake()
    {
        m_DisplayManager = GetComponent<DisplayManager>();
        m_PieceManager = GetComponent<PieceManager>();
    }

    /// <summary>
    /// 配列のデータをもとにステージを作成
    /// </summary>
    /// <param name="l_StageDesign">ステージの設計図</param>
    public void StageCreate(int[,] l_StageDesign)
    {
        if (l_StageDesign == null) { return; }
        GameObject l_StageParent = new GameObject("StageParent");
        int l_GapValue = l_StageDesign.GetLength(0) / 2;
        Vector3 l_CriteriaPos = new Vector3(l_GapValue, 0, l_GapValue);
        Vector3 l_AdjustmentValue = new Vector3(-1.5f, 0, 1.5f);

        GameObject[,] l_GameObjectList = new GameObject[l_StageDesign.GetLength(0), l_StageDesign.GetLength(1)];

        for (int x = 0; x < l_StageDesign.GetLength(0); x++)
        {           
            for (int y = 0; y < l_StageDesign.GetLength(1); y++)
            {
                Vector3    l_CurrentPos = new Vector3();
                GameObject l_CurrentPanel = Instantiate(m_Panel);
                l_CurrentPos.x = l_CriteriaPos.x - y;
                l_CurrentPos.z = l_CriteriaPos.z - x;
                l_CurrentPos.x = l_CurrentPos.x * l_AdjustmentValue.x;
                l_CurrentPos.z = l_CurrentPos.z * l_AdjustmentValue.z;
                l_CurrentPanel.transform.position = l_CurrentPos;
                l_CurrentPanel.transform.parent = l_StageParent.transform;
                l_CurrentPanel.name = "Piece" + "[" + x + "," + y + "]";
                l_GameObjectList[x, y] = l_CurrentPanel;
            }
        }
        m_DisplayManager.GameObjectListUpdate(l_GameObjectList);
    }

    /// <summary>
    /// 駒初期化
    /// </summary>
    /// <param name="l_PieceCount">駒の数</param>
    public void InitializePiecePos(int l_PieceCount, int[,] l_StageDesign)
    {
        int l_CenterValue = l_StageDesign.GetLength(0) / 2;
        Vector3 PiecePos = new Vector3(l_CenterValue, 0.5f, l_CenterValue);

        m_PieceManager.ReturnPieceObj(0).transform.position = new Vector3(0, 0.5f, PiecePos.z * -m_AdjustmentValue);


        m_PieceManager.ReturnPieceObj(1).transform.position = new Vector3(0, 0.5f, PiecePos.z * m_AdjustmentValue);
        

        if (l_PieceCount <= 2) { return; }

        m_PieceManager.ReturnPieceObj(2).transform.position = new Vector3(PiecePos.x * -m_AdjustmentValue, 0.5f, 0);


        m_PieceManager.ReturnPieceObj(3).transform.position = new Vector3(PiecePos.x * m_AdjustmentValue, 0.5f, 0);        
    }
}




        

