﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.ObjectModel;
using System;
using Constants;

public class BoardManager : MonoBehaviour
{
    //現在の盤面情報を持っている
    private int[,] m_StageInfo;

    private List<Grid> m_MovableList = new List<Grid>();

    [SerializeField]
    private Text m_LogText;

    /// <summary>
    /// ステージ情報の初期化
    /// </summary>
    /// <param name="l_DefaultStage">ステージの初期配列</param>
    public void InitializeStage(int[,]l_DefaultStage)
    {
        m_StageInfo = l_DefaultStage;
    }

    /// <summary>
    /// ステージ情報を返す
    /// </summary>
    /// <returns></returns>
    public int[,] ReturnStageInfo()
    {
        return m_StageInfo;
    }

    /// <summary>
    /// グリッドの情報を更新する
    /// </summary>
    /// <param name="l_Index">更新するマスのインデックス</param>
    /// <param name="l_GridInfo">更新後の数値</param>
    public void GridUpdate(Grid l_Index,int l_GridInfo)
    {
        m_StageInfo[l_Index.X,l_Index.Y] = l_GridInfo;
    }

    /// <summary>
    /// 移動可能マスのインデックスをリストに追加する
    /// </summary>
    /// <param name="l_Index">移動可能マス</param>
    public void AddMovableList(Grid l_Index)
    {
        m_MovableList.Add(l_Index);
    }

    /// <summary>
    /// 移動可能マスをリセットする
    /// </summary>
    public void ResetMovableGrid()
    {
        foreach (Grid l_Index in m_MovableList)
        {
            GridUpdate(l_Index, m_StageInfo[l_Index.X, l_Index.Y] & (~Bit.MOVABLE));
        }
        m_MovableList.Clear();
    }

    /// <summary>
    /// グリッド情報をテキストに書き出す
    /// </summary>
    public void GridDebug()
    {
        //テキスト初期化
        m_LogText.text = "";

        //配列書き込み
        for (int x = 0; x < m_StageInfo.GetLength(0); x++)
        {
            for (int y = 0; y < m_StageInfo.GetLength(1); y++)
            {
                m_LogText.text += m_StageInfo[x, y] + ",";
            }
            m_LogText.text += "\n";
        }
    }
}
