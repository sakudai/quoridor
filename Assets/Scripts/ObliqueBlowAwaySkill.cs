﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObliqueBlowAwaySkill : MonoBehaviour, ISkill
{
    private StateManager m_StateManager;

    private PieceManager m_PieceManager;

    private DisplayManager m_DisplayManager;

    private void Start()
    {
        m_StateManager = GameObject.FindGameObjectWithTag("System").GetComponent<StateManager>();
        m_PieceManager = GameObject.FindGameObjectWithTag("System").GetComponent<PieceManager>();
        m_DisplayManager = GameObject.FindGameObjectWithTag("System").GetComponent<DisplayManager>();
    }

    public void Use(int l_TurnNum)
    {
        StartCoroutine(BlowAway(l_TurnNum));
    }

    public IEnumerator BlowAway(int l_TurnNum)
    {
        m_StateManager.UpdateState(StateManager.TurnState.Skill);
        m_StateManager.UpdateState(StateManager.SkillState.ObliqueBlowAway);

        for (int i = 0; i < m_PieceManager.ReturnListCount(); i++)
        {

            if (i == l_TurnNum) { continue; }
            m_PieceManager.CallingObliqueMovableCheck(i);

            Grid l_MoveIndex;

            if (m_DisplayManager.ReturnMovableListEmptyFlg())
            {
                l_MoveIndex = m_PieceManager.ReturnPieceIndex(i);
            }
            else
            {
                l_MoveIndex = m_DisplayManager.ReturnRandomMovableIndex();
            }

            yield return StartCoroutine(m_PieceManager.ForciblyMove(i, l_MoveIndex));

            m_DisplayManager.MovableColorReset();
        }

        m_StateManager.UpdateState(StateManager.TurnState.End);
        yield break;
    }
}
