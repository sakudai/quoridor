﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckManager : MonoBehaviour
{
    [SerializeField]
    private GameObject m_CheckCanvas;

    private StateManager m_StateManager;

    private bool m_ReStartFlg;
    private bool m_LoopFlg;

    private void Awake()
    {
        m_StateManager = GetComponent<StateManager>();
    }

    /// <summary>
    /// 確認処理
    /// <para>ユーザーによるボタンの選択があるまで待つ</para>
    /// </summary>
    /// <param name="l_CallBack">ラムダ式でフラグに格納し、処理続行か中止か判定する</param>
    /// <returns></returns>
    public IEnumerator WaitFixed(UnityAction<bool> l_CallBack)
    {
        m_StateManager.UpdateState(StateManager.TurnState.Check);

        m_CheckCanvas.gameObject.SetActive(true);

        m_ReStartFlg = false;
        m_LoopFlg = true;

        while (m_LoopFlg)
        {
            yield return null;
        }

        m_CheckCanvas.gameObject.SetActive(false);
        l_CallBack(m_ReStartFlg);
        yield break;
    }

    /// <summary>
    /// 中止メソッド
    /// </summary>
    public void Confirm()
    {
        m_ReStartFlg = true;
        m_LoopFlg = false;
    }

    /// <summary>
    /// 確定メソッド
    /// </summary>
    public void Cancel()
    {
        m_LoopFlg = false;
    }
}
