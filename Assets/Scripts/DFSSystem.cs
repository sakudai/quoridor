﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Constants;
using System;
using UnityEngine.UI;
public class DFSSystem : MonoBehaviour
{  
    private int[,] m_StageInfo;          // 迷路
    private int[]  m_VerifiedArray;     // 確認済配列
    private int[,] m_IfSetStage;       //壁が配置されたことを想定したステージ情報
    
    private int    CheckBit;        //確認したい壁のビットを格納
    private Grid   CheckGrid;       //壁確認するためのグリッドを示す
    private int    m_PieceCount;

    private BoardManager m_BoardManager;        //ステージ情報を所有している
    private GoalManager m_GoalManager;
    private PieceManager m_PieceManager;

    private void Awake()
    {
        m_BoardManager = this.GetComponent<BoardManager>();
        m_GoalManager = this.GetComponent<GoalManager>();
        m_PieceManager = this.GetComponent<PieceManager>();
    }

    /// <summary>
    ///確認済み配列の初期化
    /// </summary>
    public void InitVerified()
    {   
        //ステージのグリッド数を知るために取得
        m_StageInfo = m_BoardManager.ReturnStageInfo();
        //ステージのグリッド数から確認済み配列の作成
        m_VerifiedArray = new int[(StageWidth) * (StageHeight)];
    }

    //ステージの横幅
    private int StageWidth
    {
        get { return this.m_StageInfo.GetLength(0); }
    }

    // ステージの高さ
    private int StageHeight
    {
        get { return this.m_StageInfo.GetLength(1); }
    }

    /// <summary>
    /// 壁がいかなる場所に置かれた場合のステージ情報の作成
    /// </summary>
    /// <param name="IfGrid">壁を置くグリッド</param>
    /// <param name="dir">true:縦配置　false:横配置</param>
    /// <returns>引数の位置と向きに対しての壁の配置可否</returns>
    public bool IfStageMake(Grid IfGrid,bool dir)
    {　
        //現在のステージ情報の取得
        m_StageInfo = m_BoardManager.ReturnStageInfo();

        //元の配列を参照型で扱いたいためコピーする
        m_IfSetStage = new int[m_StageInfo.GetLength(0), m_StageInfo.GetLength(1)];
        Array.Copy(m_StageInfo, m_IfSetStage, m_StageInfo.Length);

        //配置向きに合わせてステージ情報を作成する
        if (dir)
        {
            m_IfSetStage[IfGrid.X, IfGrid.Y] = m_IfSetStage[IfGrid.X, IfGrid.Y] | Bit.CROSS | Bit.RIGHT;
            m_IfSetStage[IfGrid.X + 1, IfGrid.Y] = m_IfSetStage[IfGrid.X + 1, IfGrid.Y] | Bit.RIGHT;
        }
        else
        {
            m_IfSetStage[IfGrid.X, IfGrid.Y] = m_IfSetStage[IfGrid.X, IfGrid.Y] | Bit.CROSS | Bit.BOTTOM;
            m_IfSetStage[IfGrid.X , IfGrid.Y+1] = m_IfSetStage[IfGrid.X, IfGrid.Y+1] | Bit.BOTTOM;
        }
        //各駒それぞれ壁の配置の可否を調べる
        for(int l_PieceNum = 0; l_PieceNum < m_PieceCount; l_PieceNum++)
        {
            if (GoalCheck(m_PieceManager.ReturnPieceIndex(l_PieceNum), l_PieceNum)) { continue; }
            return false;
        }

        return true;
    }

    /// <summary>
    /// 壁がいかなる場所に置かれた場合のステージ情報の作成(1マス壁バージョン)
    /// </summary>
    /// <param name="IfGrid">壁を置くグリッド</param>
    /// <param name="dir">true:縦配置　false:横配置</param>
    /// <returns>引数の位置と向きに対しての壁の配置可否</returns>
    public bool IFStageMakeHalfWallVersion(Grid IfGrid, bool dir)
    {
        //現在のステージ情報の取得
        m_StageInfo = m_BoardManager.ReturnStageInfo();

        //元の配列を参照型で扱いたいためコピーする
        m_IfSetStage = new int[m_StageInfo.GetLength(0), m_StageInfo.GetLength(1)];
        Array.Copy(m_StageInfo, m_IfSetStage, m_StageInfo.Length);

        //配置向きに合わせてステージ情報を作成する
        if (dir)
        {
            m_IfSetStage[IfGrid.X, IfGrid.Y] = m_IfSetStage[IfGrid.X, IfGrid.Y] | Bit.RIGHT;
        }
        else
        {
            m_IfSetStage[IfGrid.X, IfGrid.Y] = m_IfSetStage[IfGrid.X, IfGrid.Y] | Bit.BOTTOM;
        }
        //各駒それぞれ壁の配置の可否を調べる
        for (int l_PieceNum = 0; l_PieceNum < m_PieceCount; l_PieceNum++)
        {
            if (GoalCheck(m_PieceManager.ReturnPieceIndex(l_PieceNum), l_PieceNum)) { continue; }
            return false;
        }

        return true;
    }

    /// <summary>
    /// 壁がいかなる場所に置かれた場合のステージ情報の作成
    /// </summary>
    /// <param name="IfGrid">壁を置くグリッド</param>
    /// <param name="dir">true:縦配置　false:横配置</param>
    /// <returns>引数の位置と向きに対しての壁の配置可否</returns>
    public bool IfStageMakeLShapedWallVersion(Grid IfGrid, Grid IfSecondGrid, bool dir)
    {
        //現在のステージ情報の取得
        m_StageInfo = m_BoardManager.ReturnStageInfo();

        //元の配列を参照型で扱いたいためコピーする
        m_IfSetStage = new int[m_StageInfo.GetLength(0), m_StageInfo.GetLength(1)];
        Array.Copy(m_StageInfo, m_IfSetStage, m_StageInfo.Length);

        //配置向きに合わせてステージ情報を作成する
        if (dir)
        {
            m_IfSetStage[IfGrid.X, IfGrid.Y] = m_IfSetStage[IfGrid.X, IfGrid.Y] | Bit.CROSS | Bit.RIGHT;
            m_IfSetStage[IfGrid.X + 1, IfGrid.Y] = m_IfSetStage[IfGrid.X + 1, IfGrid.Y] | Bit.RIGHT;
            m_IfSetStage[IfSecondGrid.X, IfSecondGrid.Y] = m_IfSetStage[IfSecondGrid.X, IfSecondGrid.Y] | Bit.CROSS | Bit.BOTTOM;
            m_IfSetStage[IfSecondGrid.X, IfSecondGrid.Y + 1] = m_IfSetStage[IfSecondGrid.X, IfSecondGrid.Y + 1] | Bit.BOTTOM;
        }
        else
        {
            m_IfSetStage[IfGrid.X, IfGrid.Y] = m_IfSetStage[IfGrid.X, IfGrid.Y] | Bit.CROSS | Bit.BOTTOM;
            m_IfSetStage[IfGrid.X, IfGrid.Y + 1] = m_IfSetStage[IfGrid.X, IfGrid.Y + 1] | Bit.BOTTOM;
            m_IfSetStage[IfSecondGrid.X, IfSecondGrid.Y] = m_IfSetStage[IfSecondGrid.X, IfSecondGrid.Y] | Bit.CROSS | Bit.RIGHT;
            m_IfSetStage[IfSecondGrid.X + 1, IfSecondGrid.Y] = m_IfSetStage[IfSecondGrid.X + 1, IfSecondGrid.Y] | Bit.RIGHT;
        }

        //各駒それぞれ壁の配置の可否を調べる
        for (int l_PieceNum = 0; l_PieceNum < m_PieceCount; l_PieceNum++)
        {
            if (GoalCheck(m_PieceManager.ReturnPieceIndex(l_PieceNum), l_PieceNum)) { continue; }
            return false;
        }

        return true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="l_StartGrid"></param>
    /// <param name="l_PieceNum"></param>
    /// <returns></returns>
    public bool GoalCheck(Grid l_StartGrid,int l_PieceNum)
    {　
        //ゴールできるか否か
        bool isGoaled = false;
        //探索対象グリッドを格納するスタック
        var stack = new Stack<Grid>();
        //駒の現在位置の格納
        stack.Push(l_StartGrid);

        //探索済み配列の初期化(初期位置も探索済みとする)
        m_VerifiedArray = Enumerable.Repeat(-1, m_VerifiedArray.Length).ToArray();
        m_VerifiedArray[ToIndex(l_StartGrid)] = ToIndex(l_StartGrid);
       
        // 探索待ちのセルがなくなるまで続ける
        while (stack.Count > 0 && !isGoaled)
        {        
            // 探索対象のグリッドを取り出す
            Grid CurrentGrid = stack.Pop();

            // 対象のセルから上下左右のセルを探索する
            foreach (Direction dir in Enum.GetValues(typeof(Direction)))
            {
                // 次の探索セルを作成する
                Grid l_NextGrid = new Grid(CurrentGrid.X, CurrentGrid.Y);
                switch (dir)
                {
                    case Direction.Up:
                         l_NextGrid.X -= 1;
                         CheckGrid = l_NextGrid;
                         CheckBit= Bit.BOTTOM;       
                        break;
                    case Direction.Right:
                        l_NextGrid.Y+= 1;
                        CheckGrid = CurrentGrid;
                        CheckBit = Bit.RIGHT;
                        break;
                    case Direction.Down:
                        l_NextGrid.X += 1;
                        CheckGrid = CurrentGrid;
                        CheckBit = Bit.BOTTOM;
                        break;
                    case Direction.Left:
                         l_NextGrid.Y -= 1;
                         CheckGrid = l_NextGrid;
                         CheckBit = Bit.RIGHT;
                        break;
                }
                
                // 探索グリッドが範囲内
                if (l_NextGrid.X >= 0 && l_NextGrid.Y >= 0 && l_NextGrid.X < (StageWidth) && l_NextGrid.Y < (StageHeight))
                {
                   
                    // 未確認であり間に壁がなければ
                    if (m_VerifiedArray[ToIndex(l_NextGrid)] < 0
                        && (m_IfSetStage[CheckGrid.X, CheckGrid.Y] &CheckBit)==0)
                    {
                        // 探索済情報として格納する
                        SetVisited(l_NextGrid);

                        //ゴールにたどり着けるかの可否
                        if (m_GoalManager.GoalCheck(l_PieceNum,l_NextGrid))
                        {
                            //たどり着けたので他の探索も打ち切るため中身をクリアします
                            stack.Clear();
                            isGoaled = true;
                            break;
                        }
                        else
                        {
                            // スタックに格納
                            stack.Push(l_NextGrid);
                        }
                    }
                }
            }
        }
        // ゴール可能か否かを返す
        return isGoaled;
    }


    /// <summary>
    /// 駒の数受け取る
    /// </summary>
    /// <param name="l_PieceCount"></param>
    public void ReceivePieceCount(int l_PieceCount)
    {
        m_PieceCount = l_PieceCount;
    }

    // 探索済みデータの格納
    private void SetVisited(Grid nextGrid)
    {
        //var fromIndex = ToIndex(fromCell);
        var toIndex = ToIndex(nextGrid);
        m_VerifiedArray[toIndex] = 0;
    }

    // Cellを1次元配列のインデックスに変換
    private int ToIndex(Grid grid)
    {
        return grid.X + (StageWidth) * grid.Y;
    }

    // 方向
    private enum Direction
    {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3
    }

}