﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface ISkill 
{
    void Use(int i);
}