﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour
{

    private StateManager m_StateManager;

    private float m_TouchXPos;
    private float m_TouchYPos;

    private float[] m_CurrentTouchPos = new float[2];

    [SerializeField]
    private GameObject Player1Cam;
    [SerializeField]
    private GameObject Player2Cam;

    private void Start()
    {
        m_StateManager = GetComponent<StateManager>();
    }


    /// <summary>
    /// タッチされた位置を返す
    /// </summary>
    /// <returns></returns>
    public float[] TouchPosition()
    {
        //駒選択中は前のインデックス持ち越さない
        if (m_StateManager.CheckState(StateManager.TurnState.Piece))
        {
            m_TouchXPos = -1;
            m_TouchYPos = -1;
        }

        //タップ処理
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            RayCheck(ray);
        }
        //PCクリック処理
        else if(Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RayCheck(ray);
        }

        m_CurrentTouchPos[0] = m_TouchXPos;
        m_CurrentTouchPos[1] = m_TouchYPos;

        //Debug.Log("タッチインデックス:" + m_TouchXPos + "," + m_TouchYPos);

        return m_CurrentTouchPos;
    }

    private void RayCheck(Ray ray) 
    {
        RaycastHit hit = new RaycastHit();
        if(Physics.Raycast(ray,out hit))
        {
            GameObject l_TouchObj = hit.collider.gameObject;

            m_TouchXPos = hit.point.x;
            m_TouchYPos = hit.point.z;
        }
    }


    public void CameraChange(bool flag)
    {
        if (flag)
        {
            Player1Cam.SetActive(true);
            Player2Cam.SetActive(false);
        }
        else
        {
            Player1Cam.SetActive(false);
            Player2Cam.SetActive(true);
        }
    }
}
