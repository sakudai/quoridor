﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BringWall : MonoBehaviour
{
    private int[] m_WallCountArray;

    private Text[] m_WallNumArray;

    [SerializeField]
    private GameObject m_WallButton;

    [SerializeField]
    private Text m_PlayerOneWallText;
    [SerializeField]
    private Text m_PlayerTwoWallText;
    [SerializeField]
    private Text m_PlayerThirdWallText;
    [SerializeField]
    private Text m_PlayerFourthWallText;

    private const int MAX_WALL = 20;

    private int m_WallCount;

    private int m_CurrentPlayerIndex;

    private void Awake()
    {
        m_WallNumArray = new Text[] { m_PlayerOneWallText, m_PlayerTwoWallText, m_PlayerThirdWallText, m_PlayerFourthWallText };
    }

    public void SetPlayerIndex(int l_PlayerIndex)
    {
        m_CurrentPlayerIndex = l_PlayerIndex;
    }

    /// <summary>
    /// プレイヤーごとに所持する壁の個数の設定
    /// </summary>
    /// <param name="l_PlayerCount">プレイヤーの人数</param>
    public void InitializeWallCount(int l_PlayerCount)
    {
        m_WallCount = MAX_WALL / l_PlayerCount;
        m_WallCountArray = new int[l_PlayerCount];

        for (int i = 0; i < m_WallCountArray.Length; i++)
        {
            m_WallCountArray[i] = m_WallCount;
            WallCountToText(i);
        }
    }

    /// <summary>
    /// そのターンのプレイヤーが壁を所持しているかを返す
    /// </summary>
    /// <param name="l_PlayerIndex">使用したプレイヤー情報index</param>
    /// <returns></returns>
    public bool ReturnBringWall(int l_PlayerIndex)
    {
        if (m_WallCountArray[l_PlayerIndex] <= 0)
        {
            m_WallButton.SetActive(false);
            return false;
        }
        m_WallButton.SetActive(true);
        return true;
    }

    /// <summary>
    /// 壁使用時に所持数を減らす
    /// </summary>
    /// <param name="l_PlayerIndex">使用したプレイヤー情報index</param>
    public void SpendingWall()
    {  // int l_PlayerIndex
        m_WallCountArray[m_CurrentPlayerIndex] = m_WallCountArray[m_CurrentPlayerIndex] - 1;
        WallCountToText();
    }

    /// <summary>
    /// 壁の所持数をテキストに更新をかける
    /// </summary>
    public void WallCountToText()
    {
        m_WallNumArray[m_CurrentPlayerIndex].text = "Player" + (m_CurrentPlayerIndex + 1) + ": " + m_WallCountArray[m_CurrentPlayerIndex] + "";
    }
    public void WallCountToText(int l_PlayerIndex)
    {
        m_WallNumArray[l_PlayerIndex].text = "Player" + (l_PlayerIndex + 1) + ": " + m_WallCountArray[l_PlayerIndex] + "";
    }
}

