﻿#define PIECECHECK

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Constants;

public class PieceManager : MonoBehaviour
{
    private BoardManager m_BoardManager;
    private TouchManager m_TouchManager;
    private PieceMove m_PieceMove;
    private PieceMovableJudge m_PieceMovableJudge;
    private CheckManager m_CheckManager;
    private StateManager m_StateManager;
    private GoalManager m_GoalManager;
    private int[,] m_StageInfo;

    const float DitchSize = 0.5f;

    const float GridSize = 1.0f;

    private Grid m_MoveIndex = new Grid { X = -1, Y = -1 };

    [SerializeField]
    private GameObject m_MoveButton;

    [SerializeField]
    GameObject m_Piece0;

    [SerializeField]
    GameObject m_Piece1;

    [SerializeField]
    GameObject m_Piece2;

    [SerializeField]
    GameObject m_Piece3;

    private List<GameObject> m_PieceObjList;

    /// <summary>
    /// Key値:駒の番号、Value値:駒の座標
    /// </summary>
    private Dictionary<int, Grid> m_PieceIndexList = new Dictionary<int, Grid>();

    private void Awake()
    {
        m_BoardManager = GetComponent<BoardManager>();
        m_TouchManager = GetComponent<TouchManager>();
        m_PieceMove = GetComponent<PieceMove>();
        m_PieceMovableJudge = GetComponent<PieceMovableJudge>();
        m_CheckManager = GetComponent<CheckManager>();
        m_StateManager = GetComponent<StateManager>();
        m_GoalManager = GetComponent<GoalManager>();
    }

    private void Start()
    {
        m_PieceObjList = new List<GameObject>() { m_Piece0, m_Piece1, m_Piece2, m_Piece3 };
    }

    /// <summary>
    /// 駒初期配置
    /// </summary>
    /// <param name="l_PieceCount">駒の総数</param>
    public void AddListPieceIndex(int l_PieceCount)
    {
        m_StageInfo = m_BoardManager.ReturnStageInfo();
        int l_CenterValue = m_StageInfo.GetLength(0) / 2;
        Grid l_Index = new Grid();

        l_Index.X = m_StageInfo.GetLength(1) - 1;
        l_Index.Y = l_CenterValue;
        m_BoardManager.GridUpdate(l_Index, m_StageInfo[l_Index.X, l_Index.Y] | Bit.PIECE);
        m_PieceIndexList.Add(0,l_Index);

        l_Index.X = 0;
        l_Index.Y = l_CenterValue;
        m_BoardManager.GridUpdate(l_Index, m_StageInfo[l_Index.X, l_Index.Y] | Bit.PIECE);
        m_PieceIndexList.Add(1, l_Index);

        if (l_PieceCount <= 2)
        {
            m_PieceObjList.RemoveRange(2, 2);
            m_PieceIndexList.Remove(2);
            m_PieceIndexList.Remove(3);
            return;
        }

        l_Index.X = l_CenterValue;
        l_Index.Y = 0;
        m_BoardManager.GridUpdate(l_Index, m_StageInfo[l_Index.X, l_Index.Y] | Bit.PIECE);
        m_PieceIndexList.Add(2, l_Index);

        l_Index.X = l_CenterValue;
        l_Index.Y = m_StageInfo.GetLength(0) - 1;
        m_BoardManager.GridUpdate(l_Index, m_StageInfo[l_Index.X, l_Index.Y] | Bit.PIECE);
        m_PieceIndexList.Add(3, l_Index);
    }

    /// <summary>
    /// 駒のオブジェクトを返す
    /// </summary>
    /// <param name="l_PieceNum">駒の番号</param>
    /// <returns></returns>
    public GameObject ReturnPieceObj(int l_PieceNum)
    {
        return m_PieceObjList[l_PieceNum];
    }

    /// <summary>
    /// 移動可能マスチェックを呼び出す
    /// </summary>
    /// <param name="l_PieceNum">移動可能マスをチェックする駒の番号</param>
    public void CallingMovableCheck(int l_PieceNum)
    {
        m_PieceMovableJudge.ReceivePieceNum(l_PieceNum);
        m_PieceMovableJudge.MovableCheck(ReturnPieceIndex(l_PieceNum));
    }

    /// <summary>
    /// ななめ移動可能マスチェックを呼び出す
    /// </summary>
    /// <param name="l_PieceNum">移動可能マスをチェックする駒の番号</param>
    public bool CallingObliqueMovableCheck(int l_PieceNum)
    {
        m_PieceMovableJudge.ReceivePieceNum(l_PieceNum);
        return m_PieceMovableJudge.ObliqueMovableCheck(ReturnPieceIndex(l_PieceNum));
    }

    /// <summary>
    /// 移動のための準備を呼び出す
    /// </summary>
    /// <param name="l_PieceNum">駒の番号</param>
    public void CallingMoveSetup(int l_PieceNum)
    {
        Grid l_TouchIndex = IndexMake();

        //インデックスが範囲外
        if (l_TouchIndex.X == -1 || l_TouchIndex.Y == -1) { return; }

        m_StageInfo = m_BoardManager.ReturnStageInfo();

        //タッチされたマスが移動可能マスではなかったら
        if ((m_StageInfo[l_TouchIndex.X, l_TouchIndex.Y] & Bit.MOVABLE) == 0) { return; }

        //移動のための準備
        m_PieceMove.MoveSetup(l_TouchIndex, ReturnPieceObj(l_PieceNum));

        //移動ボタン表示
        m_MoveButton.SetActive(true);

        //移動先インデックスを保持
        m_MoveIndex = l_TouchIndex;
    }

    /// <summary>
    /// 移動先インデックスのリセット
    /// </summary>
    public void IndexReset()
    {
        //移動ボタン表示
        m_MoveButton.SetActive(false);

        //インデックス初期化
        m_MoveIndex = new Grid { X = -1, Y = -1 };
    }

    /// <summary>
    /// 駒移動を呼び出す
    /// </summary>
    /// <param name="l_PieceNum">駒の番号</param>
    /// <returns>移動完了フラグ</returns>
    public IEnumerator CallingMove(int l_PieceNum, UnityAction<bool> l_CallBack)
    {
        //インデックスが更新されていないなら
        if (m_MoveIndex.X == -1 || m_MoveIndex.Y == -1)
        {
            yield break;
        }

        bool l_ReStartFlg = false;
#if(!PIECECHECK)
        bool l_CheckFlg = false;

        //チェック入る
        yield return StartCoroutine(m_CheckManager.WaitFixed(b => l_CheckFlg = b));

        //中止ボタンが押されたなら中止
        if (l_CheckFlg  == false)
        {
            l_CallBack(l_ReStartFlg);
            m_StateManager.UpdateState(StateManager.TurnState.Piece);
            yield break;
        }
#endif
        bool l_WaitFlg = false;

        //移動処理
        yield return StartCoroutine(m_PieceMove.PieceObjMove(b => l_WaitFlg = b));

        while (!l_WaitFlg)
        {
            yield return null;
        }

        m_PieceMove.Move(m_MoveIndex, ReturnPieceObj(l_PieceNum), ReturnPieceIndex(l_PieceNum));
        UpdatePieceIndex(m_MoveIndex, l_PieceNum);

        //ゴールチェック
        if(m_GoalManager.GoalCheck(l_PieceNum, m_PieceIndexList[l_PieceNum]))
        {
            m_StateManager.UpdateState(StateManager.GameState.End);
        }

        l_ReStartFlg = true;
        l_CallBack(l_ReStartFlg);

        IndexReset();
        yield break;
    }

    public IEnumerator ForciblyMove(int l_PieceNum,Grid l_MoveIndex)
    {
        //移動のための準備
        m_PieceMove.MoveSetup(l_MoveIndex, ReturnPieceObj(l_PieceNum));

        bool l_WaitFlg = false;

        //移動処理
        yield return StartCoroutine(m_PieceMove.PieceObjMove(b => l_WaitFlg = b));

        while (!l_WaitFlg)
        {
            yield return null;
        }

        m_PieceMove.Move(l_MoveIndex, ReturnPieceObj(l_PieceNum), ReturnPieceIndex(l_PieceNum));
        UpdatePieceIndex(l_MoveIndex, l_PieceNum);
        m_BoardManager.ResetMovableGrid();
    }

    /// <summary>
    /// リストのカウントを返す
    /// </summary>
    /// <returns></returns>
    public int ReturnListCount()
    {
        return m_PieceObjList.Count;
    }

    /// <summary>
    /// 駒のインデックスを返す
    /// </summary>
    /// <param name="l_PieceNum">インデックスを返す駒の番号</param>
    public Grid ReturnPieceIndex(int l_PieceNum)
    {
        return m_PieceIndexList[l_PieceNum];
    }

    /// <summary>
    /// 駒のインデックス更新
    /// </summary>
    /// <param name="l_Grid">座標のインデックス</param>
    /// <param name="l_PieceNum">更新する駒の番号</param>
    public void UpdatePieceIndex(Grid l_Grid,int l_PieceNum)
    {
        m_PieceIndexList[l_PieceNum] = l_Grid;
    }

    /// <summary>
    /// マウスの示している個所から参照したいindexを取得
    /// </summary>
    public Grid IndexMake()
    {
        m_StageInfo = m_BoardManager.ReturnStageInfo();
        float[] l_TouchPos = m_TouchManager.TouchPosition();
        Grid l_Grid = new Grid() { X = -1, Y = -1 };
        if (l_TouchPos[0] == -1 || l_TouchPos[1] == -1)
        {
            return l_Grid;
        }

        float l_CheckPos = m_StageInfo.GetLength(0) / 2 * (GridSize + DitchSize + 0.125f);
        float l_CheckXPos = l_CheckPos;
        float l_CheckYPos = -l_CheckPos;

        float l_XPos = l_TouchPos[0];
        float l_ZPos = l_TouchPos[1];


        for (int i = 0; i < m_StageInfo.GetLength(0); i++)
        {
            if (l_ZPos <= l_CheckXPos && l_ZPos >= l_CheckXPos - GridSize)
            {
                l_Grid.X = i;
                break;
            }
            
            l_Grid.X = -1;
            l_CheckXPos -= (DitchSize + GridSize);
        }

        for (int i = 0; i < m_StageInfo.GetLength(0); i++)
        {
            if (l_XPos >= l_CheckYPos && l_XPos <= l_CheckYPos + GridSize)
            {
                l_Grid.Y = i;
                break;
            }

            l_Grid.Y = -1;
            l_CheckYPos += (DitchSize + GridSize);
        }
        return l_Grid;
    }
}
