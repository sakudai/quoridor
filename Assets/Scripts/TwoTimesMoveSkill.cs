﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoTimesMoveSkill : MonoBehaviour, ISkill
{
    private StateManager m_StateManager;

    private PieceManager m_PieceManager;

    private void Start()
    {
        m_StateManager = GameObject.FindGameObjectWithTag("System").GetComponent<StateManager>();
        m_PieceManager = GameObject.FindGameObjectWithTag("System").GetComponent<PieceManager>();
    }

    public void Use(int l_TurnNum)
    {
        m_PieceManager.CallingMovableCheck(l_TurnNum);
        m_StateManager.UpdateState(StateManager.TurnState.Skill);
        m_StateManager.UpdateState(StateManager.SkillState.TwoTimesMove);
    }
}
