﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private StateManager m_StateManager;

    private int m_CurrentTurnPieceNum;

    [SerializeField,Header("True:2人プレイ,False:4人プレイ")]
    private bool m_TwoPieceFlg;

    private int m_PieceCount;

    private TurnManager m_TurnManager;

    private BoardManager m_BoardManager;

    private DisplayManager m_DisplayManager;

    private PieceManager m_PieceManager;

    private WallManager m_WallManager;

    private StageManager m_StageManager;

    private CheckManager m_CheckManager;

    private GoalManager m_GoalManager;

    private SkillManager m_SkillManager;

    private DFSSystem m_DFSSystem;

    private BringWall m_BringWall;

    [SerializeField]
    private GameObject m_GameCanvas;
    [SerializeField]
    private GameObject m_DebugCanvas;
    [SerializeField]
    private GameObject m_WallNumTextPanel;
    [SerializeField]
    private GameObject m_OptionCanvas;

    private void Awake()
    {
        DecidePieceCount();
        m_StateManager = GetComponent<StateManager>();
        m_TurnManager = GetComponent<TurnManager>();
        m_BoardManager = GetComponent<BoardManager>();
        m_DisplayManager = GetComponent<DisplayManager>();
        m_PieceManager = GetComponent<PieceManager>();
        m_WallManager = GetComponent<WallManager>();
        m_StageManager = GetComponent<StageManager>();
        m_CheckManager = GetComponent<CheckManager>();
        m_GoalManager = GetComponent<GoalManager>();
        m_SkillManager = GetComponent<SkillManager>();
        m_DFSSystem = GetComponent<DFSSystem>();
        m_BringWall = GetComponent<BringWall>();
    }

    private void Start()
    {
        DecidePieceCount();
        m_SkillManager.UpdateMaxPieceNum(m_PieceCount);
    }

    private void Update()
    {   
        //SEデバッグ用
        if (Input.GetMouseButton(0)) AudioManager.Instance.PlaySE("shakin1", 0);

        if (m_StateManager.CheckState(StateManager.GameState.Ready))
        {
            //ゲーム開始時の処理
            m_GameCanvas.SetActive(true);
            m_DebugCanvas.SetActive(true);
            m_StateManager.UpdateState(StateManager.GameState.Start);
            m_DFSSystem.ReceivePieceCount(m_PieceCount);
            m_StageManager.StageInitialize(m_PieceCount);
            m_DFSSystem.InitVerified();
            m_WallManager.WallInitialPreparation();
            m_CurrentTurnPieceNum = m_TurnManager.DecidePieceOrder(m_PieceCount);
            m_GameCanvas.SetActive(true);
            m_BringWall.InitializeWallCount(m_PieceCount);
            m_DisplayManager.ReceivePlayerIndex(m_CurrentTurnPieceNum);
            m_BringWall.SetPlayerIndex(m_CurrentTurnPieceNum);
            m_DisplayManager.UpdatePlayerText(m_CurrentTurnPieceNum);
        }
        else if (m_StateManager.CheckState(StateManager.GameState.End))
        {
            //ゲーム終了時の処理
            m_GoalManager.DisplayWinNum(m_CurrentTurnPieceNum);
            m_GameCanvas.SetActive(false);
            m_StateManager.UpdateState(StateManager.GameState.Stay);
        }
        else if (m_StateManager.CheckState(StateManager.TurnState.End))
        {
            //ターン終了時の処理(次のターンの準備等)
            /*m_SkillManager.RemoveSkillCard(m_CurrentTurnPieceNum);*/
            m_CurrentTurnPieceNum = m_TurnManager.TurnChange();
            m_BringWall.SetPlayerIndex(m_CurrentTurnPieceNum);
            m_DisplayManager.ReceivePlayerIndex(m_CurrentTurnPieceNum);
            m_DisplayManager.UpdatePlayerText(m_CurrentTurnPieceNum);
            m_BoardManager.GridDebug();
            if (m_BringWall.ReturnBringWall(m_CurrentTurnPieceNum))
            {
                m_WallManager.NewVerticalCheck();
                m_WallManager.NewHorizontalCheck();
            }
            m_StateManager.UpdateState(StateManager.TurnState.Wait);

            if (!m_StateManager.CheckState(StateManager.SkillState.Stay))
            {
                m_StateManager.UpdateState(StateManager.SkillState.Stay);
            }
            m_SkillManager.ButtonDisplayCheck(m_CurrentTurnPieceNum);
        }

        if (!Input.anyKeyDown) { return; }

        //PC用クリック
        if (Input.GetMouseButtonDown(0))
        {
            if (m_StateManager.CheckState(StateManager.TurnState.Piece) || m_StateManager.CheckState(StateManager.TurnState.Skill))
            {
                m_PieceManager.CallingMoveSetup(m_CurrentTurnPieceNum);
            }
            /*else if (m_StateManager.CheckState(StateManager.TurnState.Card))
            {
                StartCoroutine(m_SkillManager.GetClickObj(m_CurrentTurnPieceNum));
            }*/
        }
        //モバイル用画面タップ
        else if (Input.touchCount > 0)
        {
            Touch l_touch = Input.GetTouch(0);

            //タップ
            if (l_touch.phase == TouchPhase.Began)
            {
                if (m_StateManager.CheckState(StateManager.TurnState.Piece) || m_StateManager.CheckState(StateManager.TurnState.Skill))
                {
                    m_PieceManager.CallingMoveSetup(m_CurrentTurnPieceNum);
                }
               /* else if (m_StateManager.CheckState(StateManager.TurnState.Card))
                {
                    StartCoroutine(m_SkillManager.GetClickObj(m_CurrentTurnPieceNum));
                }*/
            }
        }
    }

    /// <summary>
    /// 駒ボタン選択
    /// </summary>
    public void SelectPiece()
    {
        if (m_StateManager.CheckState(StateManager.TurnState.Wait))
        {
            //駒の開始処理
            SetMovable();
            m_StateManager.UpdateState(StateManager.TurnState.Piece);
        }
        else if (m_StateManager.CheckState(StateManager.TurnState.Wall))
        {
            //壁の終了処理
            m_WallManager.ChangeCanWallSetFlg(false);
            m_WallManager.WallStorage();
            m_StateManager.UpdateState(StateManager.TurnState.Wait);
            SelectPiece();
        }
        else if (m_StateManager.CheckState(StateManager.TurnState.Card))
        {
            //カードの終了処理
            m_StateManager.UpdateState(StateManager.TurnState.Wait);
            /*m_SkillManager.RemoveSkillCard(m_CurrentTurnPieceNum);*/
            SelectPiece();
        }
    }

    /// <summary>
    /// 壁ボタン選択
    /// </summary>
    public void SelectWall()
    {
        if (m_StateManager.CheckState(StateManager.TurnState.Wait))
        {
            //壁の開始処理
            m_WallManager.ChangeCanWallSetFlg(true);
            m_WallManager.UpdateStageInfo();
            m_StateManager.UpdateState(StateManager.TurnState.Wall);
        }
        else if (m_StateManager.CheckState(StateManager.TurnState.Piece))
        {
            //駒の終了処理
            ResetMovable();
            m_DisplayManager.RemovePieceShadow();
            m_PieceManager.IndexReset();
            m_StateManager.UpdateState(StateManager.TurnState.Wait);
            SelectWall();
        }
        else if (m_StateManager.CheckState(StateManager.TurnState.Card))
        {
            //カードの終了処理
            m_StateManager.UpdateState(StateManager.TurnState.Wait);
            /*m_SkillManager.RemoveSkillCard(m_CurrentTurnPieceNum);*/
            SelectWall();
        }
    }

    /// <summary>
    /// 回転ボタン選択
    /// </summary>
    public void SelectRotate()
    {
        m_WallManager.WallRotation();
    }

    /// <summary>
    /// 設置ボタン選択
    /// </summary>
    public void SelectBuild()
    {

        if (m_StateManager.CheckState(StateManager.TurnState.Wall))
        {
            //設置出来なかったら
            if (!m_WallManager.WallSet()) { return; }
        }
        else if(m_StateManager.CheckState(StateManager.SkillState.HalfWallBuild))
        {
            //設置出来なかったら
            if (!m_WallManager.HalfWallSet()) { return; }
        }
        else
        {
            //設置出来なかったら
            if (!m_WallManager.LShapedWallSet()) { return; }
        }
        m_WallManager.WallStorage();
    }

    /// <summary>
    /// スキルボタン選択
    /// </summary>
    public void SelectSkill()
    {
        if (m_StateManager.CheckState(StateManager.TurnState.Wait))
        {
            //カードの開始処理

            StartCoroutine(m_SkillManager.CallingUse(m_CurrentTurnPieceNum));

            ///m_StateManager.UpdateState(StateManager.TurnState.Card);
            ///m_SkillManager.DisplaySkillCard(m_CurrentTurnPieceNum);
        }
        else if (m_StateManager.CheckState(StateManager.TurnState.Piece))
        {
            //駒の終了処理
            ResetMovable();
            m_DisplayManager.RemovePieceShadow();
            m_PieceManager.IndexReset();
            m_StateManager.UpdateState(StateManager.TurnState.Wait);
            SelectSkill();
        }
        else if (m_StateManager.CheckState(StateManager.TurnState.Wall))
        {
            //壁の終了処理
            m_WallManager.ChangeCanWallSetFlg(false);
            m_WallManager.WallStorage();
            m_StateManager.UpdateState(StateManager.TurnState.Wait);
            SelectSkill();
        }
    }

    /// <summary>
    /// 移動ボタン選択
    /// </summary>
    public void SelectMove()
    {
        StartCoroutine(PieceMoveStart());
    }

    /// <summary>
    /// オプションボタン選択時
    /// </summary>
    public void SelectOption()
    {
        m_OptionCanvas.SetActive(true);
        AudioManager.Instance.PlayBGM("忘れた記憶", 0);
    }

    /// <summary>
    /// 駒の総数決定
    /// </summary>
    private void DecidePieceCount()
    {
        if (m_TwoPieceFlg)
        {
            m_PieceCount = 2;
        }
        else
        {
            m_PieceCount = 4;
            m_WallNumTextPanel.GetComponent<RectTransform>().localScale = 
                new Vector2(m_WallNumTextPanel.GetComponent<RectTransform>().localScale.x, 1);
        }
    }

    /// <summary>
    /// 移動可能マスを設定する
    /// </summary>
    public void SetMovable()
    {
        m_PieceManager.CallingMovableCheck(m_CurrentTurnPieceNum);
    }

    /// <summary>
    /// <para>移動可能マスのリセット</para>
    /// 駒の移動終了時や、キャンセル時に呼び出す
    /// </summary>
    private void ResetMovable()
    {
        m_BoardManager.ResetMovableGrid();
        m_DisplayManager.MovableColorReset();
    }

    /// <summary>
    /// 駒の移動メソッドを呼び出す大元メソッド
    /// </summary>
    private IEnumerator PieceMoveStart()
    {
        bool l_ReStartFlg = false;
        yield return StartCoroutine(m_PieceManager.CallingMove(m_CurrentTurnPieceNum,b => l_ReStartFlg = b));
        if (l_ReStartFlg)
        {
            m_DisplayManager.RemovePieceShadow();
            ResetMovable();
            m_StateManager.UpdateState(StateManager.TurnState.End);
            if (m_StateManager.CheckState(StateManager.SkillState.TwoTimesMove))
            {
                if (!m_SkillManager.CheckMoveCount())
                {
                    SetMovable();
                    m_StateManager.UpdateState(StateManager.TurnState.Skill);
                }
            }
        }
        yield break;
    }
}
