﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constants;

public class PieceMovableJudge : MonoBehaviour
{
    private BoardManager m_BoardManager;

    private DisplayManager m_DisplayManager;

    private int[,] m_StageInfo;

    private bool m_NotMoveFlg;

    private int m_PieceNum;

    private void Awake()
    {
        m_BoardManager = GetComponent<BoardManager>();
        m_DisplayManager = GetComponent<DisplayManager>();
    }

    /// <summary>
    /// 移動可能マスをチェックする
    /// </summary>
    /// <param name="l_Index">チェックする駒の元のインデックス</param>
    public void MovableCheck(Grid l_Index)
    {
        m_DisplayManager.ReceivePlayerIndex(m_PieceNum);
        m_StageInfo = m_BoardManager.ReturnStageInfo();


        foreach (Direction l_Dir in Enum.GetValues(typeof(Direction)))
        {
            DirectionCheck(l_Index, false, l_Dir);
        }
    }

    /// <summary>
    /// ななめの移動可能マスをチェックする
    /// </summary>
    /// <param name="l_Index">チェックする駒の元のインデックス</param>
    public bool ObliqueMovableCheck(Grid l_Index)
    {
        m_DisplayManager.ReceivePlayerIndex(m_PieceNum);
        m_StageInfo = m_BoardManager.ReturnStageInfo();

        TopLeftCheck(l_Index);
        TopRightCheck(l_Index);
        BottomLeftCheck(l_Index);
        BottomRightCheck(l_Index);

        return m_DisplayManager.ReturnMovableListEmptyFlg();
    }

    /// <summary>
    /// 指定のマスの指定ビットをチェック
    /// </summary>
    /// <param name="l_Index">指定のマスのインデックス</param>
    /// <param name="l_InspectionBit">指定ビット</param>
    /// <returns></returns>
    private bool GridBitCheck(Grid l_Index, int l_InspectionBit)
    {
        return (m_StageInfo[l_Index.X, l_Index.Y] & l_InspectionBit) == l_InspectionBit;
    }

    /// <summary>
    /// 方向
    /// </summary>
    private enum Direction
    {
        Top,
        Bottom,
        Left,
        Right
    }

    /// <summary>
    /// 駒の番号を受け取る
    /// </summary>
    /// <param name="l_PieceNum">駒の番号</param>
    public void ReceivePieceNum(int l_PieceNum)
    {
        m_PieceNum = l_PieceNum;
    }

    /// <summary>
    /// 方向チェック
    /// </summary>
    /// <param name="l_Index">マスのインデックス</param>
    /// <param name="l_SecondFlg">二段階目のチェック</param>
    /// <param name="l_Dir">チェックする方向</param>
    private void DirectionCheck(Grid l_Index, bool l_SecondFlg, Direction l_Dir)
    {
        int l_CheckBit;

        //チェックするビット決定
        if(l_Dir == Direction.Top || l_Dir == Direction.Bottom)
        {
            l_CheckBit = Bit.BOTTOM;
        }
        else
        {
            l_CheckBit = Bit.RIGHT;
        }

        if (l_Dir == Direction.Top || l_Dir == Direction.Left)
        {
            //インデックスずらす
            if (l_Dir == Direction.Top)
            {
                l_Index.X -= 1;
            }
            else
            {
                l_Index.Y -= 1;
            }

            //インデックスが範囲外の時
            if (0 > l_Index.X || 0 > l_Index.Y)
            {
                if (l_SecondFlg)
                {
                    m_NotMoveFlg = true;
                }
                return;
            }
        }

        //指定の方向に壁が立っているかチェック
        if (GridBitCheck(l_Index, l_CheckBit))
        {
            if (l_SecondFlg)
            {
                m_NotMoveFlg = true;
            }
            return;
        }

        if (l_Dir == Direction.Bottom || l_Dir == Direction.Right)
        {
            //インデックスずらす
            if (l_Dir == Direction.Bottom)
            {
                l_Index.X += 1;
            }
            else
            {
                l_Index.Y += 1;
            }

            //インデックスが範囲外の時
            if (m_StageInfo.GetLength(0) <= l_Index.X || m_StageInfo.GetLength(1) <= l_Index.Y)
            {
                if (l_SecondFlg)
                {
                    m_NotMoveFlg = true;
                }
                return;
            }
        }

        //マスに駒があるか
        if (GridBitCheck(l_Index, Bit.PIECE))
        {
            if (l_SecondFlg)
            {
                m_NotMoveFlg = true;
                return;
            }

            m_NotMoveFlg = false;

            //駒飛び越えた先チェック
            DirectionCheck(l_Index, true, l_Dir);

            //駒を飛び越えた先がいけない場合
            if (m_NotMoveFlg)
            {
                if (l_Dir == Direction.Top || l_Dir == Direction.Bottom)
                {
                    //左右のチェック
                    DirectionCheck(l_Index, true, Direction.Left);

                    DirectionCheck(l_Index, true, Direction.Right);
                }
                else
                {
                    //上下のチェック
                    DirectionCheck(l_Index, true, Direction.Top);

                    DirectionCheck(l_Index, true, Direction.Bottom);
                }
            }
            return;
        }
        MovableProcess(l_Index);
    }

    /// <summary>
    /// 左上チェック(スキル用)
    /// </summary>
    /// <param name="l_Index">現在位置</param>
    private void TopLeftCheck(Grid l_Index)
    {
        Grid l_WorkIndex = l_Index;

        l_WorkIndex.X -= 1;

        //インデックスが範囲外
        if (0 > l_WorkIndex.X) { return; }

        //指定の方向に壁が立っているかチェック
        if (!GridBitCheck(l_WorkIndex, Bit.BOTTOM))
        {
            l_WorkIndex.Y -= 1;

            //インデックスが範囲外
            if (0 > l_WorkIndex.Y) { return; }

            if (!GridBitCheck(l_WorkIndex, Bit.RIGHT))
            {
                //移動先に駒がある
                if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

                MovableProcess(l_WorkIndex);
                return;
            }
        }

        l_WorkIndex = l_Index;

        l_WorkIndex.Y -= 1;

        //インデックスが範囲外
        if (0 > l_WorkIndex.Y) { return; }

        //指定の方向に壁が立っているかチェック
        if (GridBitCheck(l_WorkIndex, Bit.RIGHT)) { return; }

        l_WorkIndex.X -= 1;

        //インデックスが範囲外
        if (0 > l_WorkIndex.X) { return; }

        if(GridBitCheck(l_WorkIndex, Bit.BOTTOM)) { return; }

        //移動先に駒がある
        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

        MovableProcess(l_WorkIndex);
    }

    /// <summary>
    /// 右上チェック(スキル用)
    /// </summary>
    /// <param name="l_Index">現在位置</param>
    private void TopRightCheck(Grid l_Index)
    {
        Grid l_WorkIndex = l_Index;

        l_WorkIndex.X -= 1;

        //インデックスが範囲外
        if (0 > l_WorkIndex.X) { return; }

        //指定の方向に壁が立っているかチェック
        if (!GridBitCheck(l_WorkIndex, Bit.BOTTOM) && !GridBitCheck(l_WorkIndex, Bit.RIGHT))
        {
            l_WorkIndex.Y += 1;

            //インデックスが範囲外
            if (m_StageInfo.GetLength(1) <= l_WorkIndex.Y) { return; }

            //移動先に駒がある
            if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

            MovableProcess(l_WorkIndex);
            return;
        }

        l_WorkIndex = l_Index;

        //指定の方向に壁が立っているかチェック
        if (GridBitCheck(l_WorkIndex, Bit.RIGHT)) { return; }

        l_WorkIndex.Y += 1;

        //インデックスが範囲外
        if (m_StageInfo.GetLength(1) <= l_WorkIndex.Y) { return; }

        l_WorkIndex.X -= 1;

        //インデックスが範囲外
        if (0 > l_WorkIndex.X) { return; }

        //指定の方向に壁が立っているかチェック
        if (GridBitCheck(l_WorkIndex, Bit.BOTTOM)) { return; }

        //移動先に駒がない
        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

        MovableProcess(l_WorkIndex);
    }

    /// <summary>
    /// 左下チェック(スキル用)
    /// </summary>
    /// <param name="l_Index">現在位置</param>
    private void BottomLeftCheck(Grid l_Index)
    {
        Grid l_WorkIndex = l_Index;

        //指定の方向に壁が立っているかチェック
        if (!GridBitCheck(l_WorkIndex, Bit.BOTTOM))
        {
            l_WorkIndex.X += 1;

            if(m_StageInfo.GetLength(0) <= l_WorkIndex.X) { return; }

            l_WorkIndex.Y -= 1;

            //インデックスが範囲外
            if (0 > l_WorkIndex.Y) { return; }

            if (!GridBitCheck(l_WorkIndex, Bit.RIGHT))
            {
                //移動先に駒がある
                if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

                MovableProcess(l_WorkIndex);
                return;
            }
        }

        l_WorkIndex = l_Index;

        l_WorkIndex.Y -= 1;

        //インデックスが範囲外
        if (0 > l_WorkIndex.Y) { return; }

        //指定の方向に壁が立っているかチェック
        if (GridBitCheck(l_WorkIndex, Bit.RIGHT)) { return; }

        if (GridBitCheck(l_WorkIndex, Bit.BOTTOM)) { return; }

        l_WorkIndex.X += 1;

        //インデックスが範囲外
        if (m_StageInfo.GetLength(0) <= l_WorkIndex.X) { return; }

        //移動先に駒がある
        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

        MovableProcess(l_WorkIndex);
    }

    /// <summary>
    /// 右下チェック(スキル用)
    /// </summary>
    /// <param name="l_Index">現在位置</param>
    private void BottomRightCheck(Grid l_Index)
    {
        Grid l_WorkIndex = l_Index;

        //指定の方向に壁が立っているかチェック
        if (!GridBitCheck(l_WorkIndex, Bit.BOTTOM))
        {
            l_WorkIndex.X += 1;

            //インデックスが範囲外
            if (m_StageInfo.GetLength(0) <= l_WorkIndex.X) { return; }

            if (!GridBitCheck(l_WorkIndex, Bit.RIGHT))
            {
                l_WorkIndex.Y += 1;

                //インデックスが範囲外
                if (m_StageInfo.GetLength(1) <= l_WorkIndex.Y) { return; }

                //移動先に駒がある
                if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

                MovableProcess(l_WorkIndex);
                return;
            }
        }

        l_WorkIndex = l_Index;

        //指定の方向に壁が立っているかチェック
        if (GridBitCheck(l_WorkIndex, Bit.RIGHT)) { return; }

        l_WorkIndex.Y += 1;

        //インデックスが範囲外
        if (m_StageInfo.GetLength(1) <= l_WorkIndex.Y) { return; }

        if (GridBitCheck(l_WorkIndex, Bit.BOTTOM)) { return; }

        l_WorkIndex.X += 1;

        //インデックスが範囲外
        if ((m_StageInfo.GetLength(0) <= l_WorkIndex.X)) { return; }

        //移動先に駒がない
        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

        MovableProcess(l_WorkIndex);
    }

    /// <summary>
    /// ななめ方向チェック
    /// </summary>
    /// <param name="l_Index">マスのインデックス</param>
    /// <param name="l_Dir">チェックする上下方向</param>
    /// <param name="l_SecondDir">チェックする左右方向</param>
    private void ObliqueDirectionCheck(Grid l_Index, Direction l_Dir, Direction l_SecondDir)
    {
        Grid l_WorkIndex = l_Index;

        if (l_Dir == Direction.Top)
        {
            l_WorkIndex.X -= 1;

            //インデックスが範囲外
            if (0 > l_WorkIndex.X) { return; }

            //指定の方向に壁が立っているかチェック
            if (!GridBitCheck(l_WorkIndex, Bit.BOTTOM))
            {
                if (l_SecondDir == Direction.Right)
                {
                    if (!GridBitCheck(l_WorkIndex, Bit.RIGHT))
                    {
                        l_WorkIndex.Y += 1;

                        //インデックスが範囲外
                        if (m_StageInfo.GetLength(1) <= l_WorkIndex.Y) { return; }

                        //移動先に駒がある
                        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

                        MovableProcess(l_WorkIndex);

                        return;
                    }
                }
                else if (l_SecondDir == Direction.Left)
                {
                    l_WorkIndex.Y -= 1;

                    //インデックスが範囲外
                    if (0 > l_WorkIndex.Y) { return; }

                    if (!GridBitCheck(l_WorkIndex, Bit.RIGHT))
                    {
                        //移動先に駒がある
                        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

                        MovableProcess(l_WorkIndex);

                        return;
                    }
                }
            }
        }
        else
        {
            //指定の方向に壁が立っているかチェック
            if (!GridBitCheck(l_WorkIndex, Bit.BOTTOM))
            {
                l_WorkIndex.X += 1;

                //インデックスが範囲外
                if (m_StageInfo.GetLength(0) <= l_WorkIndex.X) { return; }

                if (l_SecondDir == Direction.Right)
                {
                    if (!GridBitCheck(l_WorkIndex, Bit.RIGHT))
                    {
                        l_WorkIndex.Y += 1;
                        //インデックスが範囲外
                        if (m_StageInfo.GetLength(1) <= l_WorkIndex.Y) { return; }

                        //移動先に駒がある
                        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

                        MovableProcess(l_WorkIndex);

                        return;


                    }
                }
                else if (l_SecondDir == Direction.Left)
                {
                    l_WorkIndex.Y -= 1;

                    //インデックスが範囲外
                    if (0 > l_WorkIndex.Y) { return; }

                    if (!GridBitCheck(l_WorkIndex, Bit.RIGHT))
                    {
                        //移動先に駒がある
                        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

                        MovableProcess(l_WorkIndex);

                        return;
                    }
                }
            }
        }

        l_WorkIndex = l_Index;

        if (l_SecondDir == Direction.Left)
        {
            l_WorkIndex.Y -= 1;

            //インデックスが範囲外
            if (0 > l_WorkIndex.Y) { return; }

            //指定の方向に壁が立っているかチェック
            if (!GridBitCheck(l_WorkIndex, Bit.RIGHT))
            {
                if (l_Dir == Direction.Top)
                {
                    if (!GridBitCheck(l_WorkIndex, Bit.BOTTOM))
                    {
                        l_WorkIndex.X -= 1;
                        //インデックスが範囲外
                        if (m_StageInfo.GetLength(0) <= l_WorkIndex.X) { return; }

                        //移動先に駒がある
                        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }
                        MovableProcess(l_WorkIndex);

                        return;
                    }
                }
                else if (l_Dir == Direction.Bottom)
                {
                    l_WorkIndex.X += 1;

                    //インデックスが範囲外
                    if (0 > l_WorkIndex.X) { return; }

                    if (!GridBitCheck(l_WorkIndex, Bit.BOTTOM))
                    {
                        //移動先に駒がある
                        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

                        MovableProcess(l_WorkIndex);

                        return;
                    }
                }
            }
        }
        else
        {
            //指定の方向に壁が立っているかチェック
            if (!GridBitCheck(l_WorkIndex, Bit.RIGHT))
            {
                l_WorkIndex.Y += 1;
                //インデックスが範囲外
                if (m_StageInfo.GetLength(1) <= l_WorkIndex.Y) { return; }

                if (l_Dir == Direction.Top)
                {
                    if (!GridBitCheck(l_WorkIndex, Bit.BOTTOM))
                    {
                        l_WorkIndex.X += 1;

                        //インデックスが範囲外
                        if (m_StageInfo.GetLength(0) <= l_WorkIndex.X) { return; }

                        //移動先に駒がない
                        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

                        MovableProcess(l_WorkIndex);

                        return;
                    }
                }
                else if (l_Dir == Direction.Bottom)
                {
                    l_WorkIndex.X -= 1;

                    //インデックスが範囲外
                    if (0 > l_WorkIndex.X) { return; }

                    if (!GridBitCheck(l_WorkIndex, Bit.BOTTOM))
                    {
                        //移動先に駒がない
                        if (GridBitCheck(l_WorkIndex, Bit.PIECE)) { return; }

                        MovableProcess(l_WorkIndex);

                        return;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 移動可能マスの処理
    /// </summary>
    /// <param name="l_Index">移動先のインデックス</param>
    private void MovableProcess(Grid l_Index)
    {
        //移動可能Bitを立てる
        int l_UpdateBit = m_StageInfo[l_Index.X, l_Index.Y] | Bit.MOVABLE;
        m_BoardManager.GridUpdate(l_Index, l_UpdateBit);

        //移動可能マスの色変える
        m_BoardManager.AddMovableList(l_Index);
        m_DisplayManager.MovableColorSet(l_Index);
    }
}