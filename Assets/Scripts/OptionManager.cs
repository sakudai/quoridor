﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionManager : MonoBehaviour
{
    [SerializeField]
    private GameObject m_OptionCanvas;

   

    public void CloseOption()
    {
        m_OptionCanvas.SetActive(false);
        AudioManager.Instance.CommitVolume();
    }
}
