﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayManager : MonoBehaviour
{
    private BoardManager m_BoardManager;

    private GameObject[,] m_StageObjects;

    private List<Grid> m_MovableIndexList = new List<Grid>();

    private readonly Color32 ViolationWallColor= new Color32(200, 0, 0, 200);

    private readonly Color32 NormalWallColor = new Color32(101, 57, 12, 200);

    private readonly Color32 m_Piece0Color = new Color32(0, 0, 255, 255);

    private readonly Color32 m_Piece1Color = new Color32(255, 0, 0, 255);

    private readonly Color32 m_Piece2Color = new Color32(255, 255, 0, 255);

    private readonly Color32 m_Piece3Color = new Color32(173, 255, 47, 255);

    [SerializeField]
    GameObject m_Piece0Shadow;

    [SerializeField]
    GameObject m_Piece1Shadow;

    [SerializeField]
    GameObject m_Piece2Shadow;

    [SerializeField]
    GameObject m_Piece3Shadow;

    [SerializeField]
    private Text m_NowPlayerText;

    private List<GameObject> m_PieceShadowObjList;

    private List<Color32> m_PieceColorList;

    private int m_PieceNum;

    private void Start()
    {
        m_PieceShadowObjList = new List<GameObject>() { m_Piece0Shadow, m_Piece1Shadow, m_Piece2Shadow, m_Piece3Shadow };
        m_PieceColorList = new List<Color32>() { m_Piece0Color, m_Piece1Color, m_Piece2Color, m_Piece3Color };
    }

    /// <summary>
    /// ステージオブジェクト配列の更新
    /// </summary>
    /// <param name="l_ObjectList">オブジェクトの配列</param>
    public void GameObjectListUpdate(GameObject[,] l_ObjectList)
    {
        m_StageObjects = l_ObjectList;
    }

    /// <summary>
    /// ステージオブジェクトを返す
    /// </summary>
    /// <param name="l_Index">マスのインデックス</param>
    /// <returns></returns>
    public GameObject ReturnStageObj(Grid l_Index)
    {
        return m_StageObjects[l_Index.X, l_Index.Y];
    }

    /// <summary>
    /// 移動可能マスの色を変える
    /// </summary>
    /// <param name="l_Index">マスのインデックス</param>
    public void MovableColorSet(Grid l_Index)
    {
        Color32 l_ChangeColor = m_PieceColorList[m_PieceNum];
        m_StageObjects[l_Index.X, l_Index.Y].gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", l_ChangeColor);
        m_MovableIndexList.Add(l_Index);
    }

    /// <summary>
    /// 移動可能マスの色を元に戻す
    /// </summary>
    public void MovableColorReset()
    {
        foreach (Grid l_Index in m_MovableIndexList)
        {
            m_StageObjects[l_Index.X, l_Index.Y].gameObject.GetComponent<Renderer>().material.SetColor("_BaseColor", Color.gray);
        }
        m_MovableIndexList.Clear();
    }

    /// <summary>
    /// 移動可能マスリストが空ならtrueを返す
    /// </summary>
    /// <returns></returns>
    public bool ReturnMovableListEmptyFlg()
    {
        return m_MovableIndexList.Count == 0;
    }

    /// <summary>
    /// 移動可能マスリストからランダムに一つのインデックスを返す
    /// </summary>
    /// <returns></returns>
    public Grid ReturnRandomMovableIndex()
    {
        return m_MovableIndexList[Random.Range(0, m_MovableIndexList.Count)];
    }

    /// <summary>
    /// 壁の色変更
    /// </summary>
    /// <param name="l_Wall">タッチしている位置への壁の色</param>
    /// <param name="CanSetWall">壁の配置の可否</param>
    public void SetWallColor(GameObject l_Wall, bool CanSetWall)
    {
        if (CanSetWall)
        {
            l_Wall.GetComponent<Renderer>().material.SetColor("_BaseColor", NormalWallColor);
            return;
        }
        l_Wall.GetComponent<Renderer>().material.SetColor("_BaseColor", ViolationWallColor);
    }

    /// <summary>
    /// L字型壁の色変更
    /// </summary>
    /// <param name="l_Wall">タッチしている位置への壁の色</param>
    /// <param name="CanSetWall">壁の配置の可否</param>
    public void SetLShapedWallColor(GameObject l_Wall, bool CanSetWall)
    {
        if (CanSetWall)
        {
            foreach (Transform child in l_Wall.transform)
            {
                child.GetComponent<Renderer>().material.SetColor("_BaseColor", NormalWallColor);
            }
            return;
        }

        foreach (Transform child in l_Wall.transform)
        {
            child.GetComponent<Renderer>().material.SetColor("_BaseColor", ViolationWallColor);
        }
    }

    /// <summary>
    /// 駒番号を受け取る
    /// </summary>
    /// <param name="l_PieceNum"></param>
    public void ReceivePlayerIndex(int l_PieceNum)
    {
        m_PieceNum = l_PieceNum;
    }

    /// <summary>
    /// 移動先に半透明の駒を表示する
    /// </summary>
    /// <param name="l_SpawnPos"></param>
    public void SpawnPieceShadow(Vector3 l_SpawnPos)
    {
        m_PieceShadowObjList[m_PieceNum].transform.position = l_SpawnPos;
        m_PieceShadowObjList[m_PieceNum].SetActive(true);
    }

    /// <summary>
    /// 半透明の駒を非表示にする
    /// </summary>
    public void RemovePieceShadow()
    {
        m_PieceShadowObjList[m_PieceNum].SetActive(false);
    }

    /// <summary>
    /// 今のプレイヤーがだれかを示すTextの更新
    /// </summary>
    /// <param name="l_PlayerNum">現在のプレイヤー番号</param>
    public void UpdatePlayerText(int l_PlayerNum)
    {
        Color32 l_ChangeColor = m_PieceColorList[m_PieceNum];
        m_NowPlayerText.text = (l_PlayerNum+1).ToString() + "Pのターン";
        m_NowPlayerText.color = l_ChangeColor;
    }
}
