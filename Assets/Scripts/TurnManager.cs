﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    private List<int> m_PieceTurnList = new List<int> { 0, 2, 1, 3 };

    private int m_TurnIndex;

    private StateManager m_StateManager;

    private void Awake()
    {
        m_StateManager = GetComponent<StateManager>();
    }

    /// <summary>
    /// ターンの順番決定(スタートの人から時計回り)
    /// </summary>
    /// <param name="l_PieceCount">駒の総数</param>
    /// <returns>スタート番号</returns>
    public int DecidePieceOrder(int l_PieceCount)
    {
        //2人プレイの時2と3を削除する
        m_PieceTurnList.RemoveAll(n => (n >= l_PieceCount));

        //ランダムにスタートの番号を決める
        int l_StartNum = UnityEngine.Random.Range(0, l_PieceCount);

        //スタートの番号を先頭のリストに並び替える
        int l_StartNumIndex = m_PieceTurnList.IndexOf(l_StartNum);
        List<int> l_WorkList = m_PieceTurnList.GetRange(0, l_StartNumIndex);
        m_PieceTurnList.AddRange(l_WorkList);
        m_PieceTurnList.RemoveRange(0, l_StartNumIndex);

        //Stateを変更する
        TurnStateChange();
        m_StateManager.UpdateState(StateManager.TurnState.Wait);
        return l_StartNum;
    }

    /// <summary>
    /// 次のターンに変える
    /// </summary>
    /// <returns>変更後のターンの番号</returns>
    public int TurnChange()
    {
        if(m_TurnIndex == m_PieceTurnList.Count - 1)
        {
            m_TurnIndex = 0;
        }
        else
        {
            m_TurnIndex++;
        }
        TurnStateChange();
        return m_PieceTurnList[m_TurnIndex];
    }

    /// <summary>
    /// ターン状態の変更
    /// </summary>
    private void TurnStateChange()
    {
        m_StateManager.UpdateState((StateManager.GameState)Enum.Parse(typeof(StateManager.GameState), "Piece" + m_PieceTurnList[m_TurnIndex].ToString()));
    }
}
