﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Constants
{
    public static class Bit
    {
        //マス目の情報管理するビットマスク
        public const int NONE = 0;
        public const int PIECE = 1;
        public const int BOTTOM = 1 << 1;
        public const int RIGHT = 1 << 2;
        public const int MOVABLE = 1 << 3;
        public const int CROSS = 1 << 4;
    }
}

public struct Grid
{
    public int X { get; set; }
    public int Y { get; set; }

    public Grid(int x, int y)
    {
        this.X = x;
        this.Y = y;
    }
}

