﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constants;
using System.Linq;

public class WallManager : MonoBehaviour
{
    private BoardManager m_BoardManager;
    private TouchManager m_TouchManager;
    private CheckManager m_CheckManager;
    private StateManager m_StateManager;
    private DisplayManager m_DisplayManager;
    private DFSSystem    m_DfsSystem;
    private BringWall m_BringWall;

    private const bool VERTICAL = true;
    private const bool HORIZONTAl = false;

    private float[] m_TouchPos = new float[2];
    private int[]   m_VerticalPossible = new int[64];
    private int[]   m_HorizontalPossible = new int[64];

    private int[] m_HalfWallVerticalPossible = new int[81];
    private int[] m_HalfWallHorizontalPossible = new int[81];

    private int[] m_TopLeftPossible = new int[64];
    private int[] m_TopRightPossible = new int[64];
    private int[] m_BottomRightPossible = new int[64];
    private int[] m_BottomLeftPossible = new int[64];

    const float AdjustmentValue = 1.5f;

    private Grid m_Index = new Grid();
    //L字型壁用の2個目の壁のインデックス
    private Grid m_SecondIndex = new Grid();

    private int[,] m_StageInfo;

    private float m_CheckPos;
    private float m_CheckXPos;
    private float m_CheckYPos;
    private const float BUILD_SPEED = 0.3f;

    private Vector3 m_CurrentPos = new Vector3(0, 0.5f, 0);
    private int m_GapValue;
    private Vector3 m_CriteriaPos;
    private Vector3 m_AdjustmentValue = new Vector3(-1.5f, 0, 1.5f);

    [SerializeField] private GameObject m_Wall;
    [SerializeField] private GameObject m_HalfWall;
    [SerializeField] private GameObject m_LShapedWall;
    [SerializeField] private GameObject m_WallObj;
    [SerializeField] private GameObject m_HalfWallObj;
    [SerializeField] private GameObject m_LShapedWallObj;

    //壁の配置向きを表すフラグ　true:縦　false:横
    private bool m_WallDirectionFlag = false;
    //L字型の壁の配置向き0～3の4段階
    private int m_LShapedWallDirection;

    //壁の配置を行っているか
    private bool m_CanWallSetFlg = false;
    private bool m_CanHalfWallSetFlg = false;
    private bool m_CanLShapedWallSetFlg = false;

    private bool m_WallSetViolationFlg = true;

    //プレイヤーチェンジ壁位置調整
    private bool PlayChange = true;
    [SerializeField] Camera m_Player1Cam;
    [SerializeField] Camera m_Player2Cam;

    [SerializeField]
    private GameObject m_RotateButton;

    [SerializeField]
    private GameObject m_BuildButton;

    private void Awake()
    {
        m_TouchManager   = this.GetComponent<TouchManager>();
        m_BoardManager   = this.GetComponent<BoardManager>();
        m_CheckManager   = this.GetComponent<CheckManager>();
        m_StateManager   = this.GetComponent<StateManager>();
        m_DisplayManager = this.GetComponent<DisplayManager>();
        m_DfsSystem      = this.GetComponent<DFSSystem>();
        m_BringWall      = this.GetComponent<BringWall>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PlayChange = !PlayChange;
            TestDisplayWall(PlayChange);

        }

        if (m_CanWallSetFlg)
        {
            IndexMake();
            WallCheck();
        }

        if (m_CanHalfWallSetFlg)
        {
            HalfWallIndexMake();
            HalfWallCheck();
        }

        if (m_CanLShapedWallSetFlg)
        {
            LShapedWallIndexMake();
            LShapedWallCheck();
        }
    }

    private void TestDisplayWall(bool flag)
    {
        if (flag)
        {
            m_CheckXPos = m_CheckPos -1;
        }
        else
        { 
            m_CheckXPos = m_CheckPos + 1;
        }
        m_TouchManager.CameraChange(PlayChange);
    }


    public void WallInitialPreparation()
    {
        UpdateStageInfo();
        m_VerticalPossible = Enumerable.Repeat(0, m_VerticalPossible.Length).ToArray();
        m_HorizontalPossible = Enumerable.Repeat(0, m_VerticalPossible.Length).ToArray();
        m_HalfWallVerticalPossible = Enumerable.Repeat(0, m_HalfWallVerticalPossible.Length).ToArray();
        m_HalfWallHorizontalPossible = Enumerable.Repeat(0, m_HalfWallHorizontalPossible.Length).ToArray();
        NewVerticalCheck();
        NewHorizontalCheck();
        m_GapValue = m_StageInfo.GetLength(0) / 2;
        m_CheckPos = m_GapValue * AdjustmentValue;
        m_CheckXPos = m_CheckPos +1;
        m_CheckYPos = -m_CheckPos;

        m_CriteriaPos = new Vector3(m_GapValue, 0, m_GapValue);
        TestDisplayWall(PlayChange);
    }

    /// <summary>
    /// 壁のオブジェクトをステージの下に隠す
    /// </summary>
    public void WallStorage()
    {
        m_Wall.transform.position = new Vector3(-0.6f, -4.5f, -1f);
        m_HalfWall.transform.position = new Vector3(-0.6f, -4.5f, -2f);
        m_LShapedWall.transform.position = new Vector3(-0.6f, -4.5f, -3f);
        m_LShapedWallDirection = 0;
        m_LShapedWall.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
        if (m_WallDirectionFlag)
        {
            WallRotation();
        }
    }

    /// <summary>
    /// 壁が配置可能状態を切り替える
    /// </summary>
    public void ChangeCanWallSetFlg(bool l_Flg)
    {
        m_CanWallSetFlg = l_Flg;

        m_RotateButton.SetActive(l_Flg);
        m_BuildButton.SetActive(l_Flg);
    }

    /// <summary>
    /// ステージ情報の更新
    /// </summary>
    public void UpdateStageInfo()
    {
        m_StageInfo = m_BoardManager.ReturnStageInfo();
    }

    /// <summary>
    /// 壁の向き変更
    /// </summary>
    public void WallRotation()
    {
        if (m_CanLShapedWallSetFlg)
        {
            LShapedWallRotation();
        }
        m_WallDirectionFlag = !m_WallDirectionFlag;
        m_Wall.transform.Rotate(new Vector3(0f, m_Wall.transform.rotation.y + 90, 0f));
        m_HalfWall.transform.Rotate(new Vector3(0f, m_HalfWall.transform.rotation.y + 90, 0f));
    }

    /// <summary>
    /// 壁の配置ができるかをチェックする
    /// </summary>
    public void WallCheck()
    {
        if (m_WallDirectionFlag)
        {
            ViolationWall(m_VerticalPossible);
        }
        else
        {
            ViolationWall(m_HorizontalPossible);
        }
    }

    /// <summary>
    /// 壁設置
    /// </summary>
    /// <returns>設置完了ならtrue返す</returns>
    public bool WallSet()
    {
        if (m_WallSetViolationFlg) { return false; }

        if (m_WallDirectionFlag)
        {
            VerticalSet();
        }
        else
        {
            HorizontalSet();
        }
        m_BringWall.SpendingWall();
        return true;
    }

    /// <summary>
    /// 縦に壁が置けるindexを配列に格納
    /// </summary>
    public void NewVerticalCheck()
    {
        m_StageInfo = m_BoardManager.ReturnStageInfo();

        for (int x = 0; x < m_StageInfo.GetLength(0) - 1; x++)
        {
            for (int y = 0; y < m_StageInfo.GetLength(1) - 1; y++)
            {
                if (((m_StageInfo[x, y] | m_StageInfo[x + 1, y]) & Bit.RIGHT) != 0 || CrossCheck(new Grid(x, y))) { continue; }

                if (!m_DfsSystem.IfStageMake(new Grid(x, y), VERTICAL)) { continue; }

                SetVisited(new Grid(x, y), m_VerticalPossible);
            }
        }  
    }

    /// <summary>
    /// 横に壁が置けるindexを配列に格納
    /// </summary>
    public void NewHorizontalCheck()
    {
        m_StageInfo = m_BoardManager.ReturnStageInfo();

        for (int x = 0; x < m_StageInfo.GetLength(0) - 1; x++)
        {
            for (int y = 0; y < m_StageInfo.GetLength(1) - 1; y++)
            {
                if (((m_StageInfo[x, y] | m_StageInfo[x, y + 1]) & Bit.BOTTOM) != 0 || CrossCheck(new Grid(x, y))) { continue; }

                if (!m_DfsSystem.IfStageMake(new Grid(x, y), HORIZONTAl)) { continue; }

                SetVisited(new Grid(x, y), m_HorizontalPossible);
            }
        }
    }

    /// <summary>
    /// 縦の壁の配置
    /// </summary>
    private void VerticalSet()
    {
        StartCoroutine(DisplayWallSet());
        int l_UpdateBit;

        l_UpdateBit = Bit.RIGHT | Bit.CROSS | m_StageInfo[m_Index.X, m_Index.Y];
        m_BoardManager.GridUpdate(m_Index, l_UpdateBit);

        Grid l_Index = new Grid(m_Index.X + 1, m_Index.Y);

        l_UpdateBit = Bit.RIGHT | m_StageInfo[l_Index.X, l_Index.Y];
        m_BoardManager.GridUpdate(l_Index, l_UpdateBit);

        ChangeCanWallSetFlg(false);
    }

    /// <summary>
    /// 横の壁の配置
    /// </summary>
    private void HorizontalSet()
    {
        StartCoroutine(DisplayWallSet());
        int l_UpdateBit;

        l_UpdateBit = Bit.BOTTOM | Bit.CROSS | m_StageInfo[m_Index.X, m_Index.Y];
        m_BoardManager.GridUpdate(m_Index, l_UpdateBit);

        Grid l_Index = new Grid(m_Index.X, m_Index.Y + 1);

        l_UpdateBit = Bit.BOTTOM | m_StageInfo[l_Index.X, l_Index.Y];
        m_BoardManager.GridUpdate(l_Index, l_UpdateBit);

        ChangeCanWallSetFlg(false);
    }

    // 壁設置可能位置の設定を行う
    private void SetVisited(Grid possibleGrid, int[] PossibleArray)
    {
        int l_PossibleIndex = ToIndex(possibleGrid);
        PossibleArray[l_PossibleIndex] = -1;
    }

    // Cellを1次元配列のインデックスに変換
    private int ToIndex(Grid grid)
    {
        return grid.X + (m_StageInfo.GetLength(0) - 1) * grid.Y;
    }

    // 1次元配列のインデックスをセルに変換
    private Grid ToCell(int index)
    {
        return new Grid(index % m_StageInfo.GetLength(0), index / m_StageInfo.GetLength(0));
    }

    /// <summary>
    /// 与えられた指標の右下の交差点に壁があるかを確かめる
    /// </summary>
    /// <param name="l_Index">指標</param>
    /// <returns>true:壁がある　false:壁がない</returns>
    private bool CrossCheck(Grid l_Index)
    {
        return (m_StageInfo[l_Index.X, l_Index.Y] & Bit.CROSS) == Bit.CROSS;
    }
    
    /// <summary>
    /// マウスの示している個所から参照したいindexを取得
    /// </summary>
    public void IndexMake()
    {
        m_TouchPos = m_TouchManager.TouchPosition();

        float l_XPos = m_TouchPos[0];
        float l_ZPos = m_TouchPos[1];

        for(int i=0; i < m_StageInfo.GetLength(0) - 1; i++)
        {
            m_CheckXPos -= AdjustmentValue;
            if (l_ZPos > m_CheckXPos)
            {
                m_Index.X = i;
                TestDisplayWall(PlayChange);
                break;
            }
        }
        for (int i = 0; i < m_StageInfo.GetLength(0) - 1; i++)
        {
            m_CheckYPos += AdjustmentValue;
            if (l_XPos < m_CheckYPos)
            {
                m_Index.Y = i;
                m_CheckYPos = -m_CheckPos;
                break;
            }
        }
        MoveWall();
    }

    /// <summary>
    ///壁を配置の可否によって色変え、配置可能にさせる
    /// </summary>
    /// <param name="CheckArray">配置の可否を格納した配列</param>
    private void ViolationWall(int[] CheckArray)
    {
        int CheckIndex = ToIndex(m_Index);

        if (CheckArray[CheckIndex] != -1)
        {     
            m_WallSetViolationFlg = true;
            m_DisplayManager.SetWallColor(m_Wall, false);
            return;
        }
        m_WallSetViolationFlg = false;
        m_DisplayManager.SetWallColor(m_Wall, true);
    }

    /// <summary>
    /// 壁の配置位置の視覚化
    /// </summary>
    private void MoveWall()
    {
        if (m_CanWallSetFlg)
        {
            m_CurrentPos.x = ((m_CriteriaPos.x - m_Index.Y) * m_AdjustmentValue.x) + 0.75f;
            m_CurrentPos.z = ((m_CriteriaPos.z - m_Index.X) * m_AdjustmentValue.z) - 0.75f;
            m_Wall.transform.position = m_CurrentPos;
        }
        else if(m_CanHalfWallSetFlg)
        {
            if (m_WallDirectionFlag)
            {
                m_CurrentPos.x = ((m_CriteriaPos.x - m_Index.Y) * m_AdjustmentValue.x) + 0.75f;
                m_CurrentPos.z = ((m_CriteriaPos.z - m_Index.X) * m_AdjustmentValue.z);
            }
            else
            {
                m_CurrentPos.x = ((m_CriteriaPos.x - m_Index.Y) * m_AdjustmentValue.x);
                m_CurrentPos.z = ((m_CriteriaPos.z - m_Index.X) * m_AdjustmentValue.z) - 0.75f;
            }
            m_HalfWall.transform.position = m_CurrentPos;
        }
        else
        {
            m_CurrentPos.x = ((m_CriteriaPos.x - m_Index.Y) * m_AdjustmentValue.x) + 0.75f;
            m_CurrentPos.z = ((m_CriteriaPos.z - m_Index.X) * m_AdjustmentValue.z) - 0.75f;
            m_LShapedWall.transform.position = m_CurrentPos;
        }
    }

    /// <summary>
    /// 壁をステージ上に配置
    /// </summary>
    private IEnumerator DisplayWallSet()
    { 
        m_StateManager.UpdateState(StateManager.TurnState.Stay);

        GameObject l_CurrentWall;
        Vector3 l_SetPos;
        Vector3 l_InitSetPos;
        if (m_CanWallSetFlg)
        {
            l_CurrentWall = Instantiate(m_WallObj);
            l_SetPos = m_Wall.transform.position;
            l_InitSetPos = m_Wall.transform.position;
            if (m_WallDirectionFlag)
            {
                l_CurrentWall.transform.Rotate(new Vector3(0f, 90, 0f));
            }
        }
        else if(m_CanHalfWallSetFlg)
        {
            l_CurrentWall = Instantiate(m_HalfWallObj);
            l_SetPos = m_HalfWall.transform.position;
            l_InitSetPos = m_HalfWall.transform.position;
            if (m_WallDirectionFlag)
            {
                l_CurrentWall.transform.Rotate(new Vector3(0f, 90, 0f));
            }
        }
        else
        {
            l_CurrentWall = Instantiate(m_LShapedWallObj);
            l_SetPos = m_LShapedWall.transform.position;
            l_InitSetPos = m_LShapedWall.transform.position;
            l_CurrentWall.transform.Rotate(new Vector3(0f, 90f * m_LShapedWallDirection, 0f));
        }

        l_InitSetPos.y -= 1;
        l_CurrentWall.transform.position = l_InitSetPos;

        float l_DistanceRate = 0;

        while (l_DistanceRate<1)
        {
            l_CurrentWall.transform.position=Vector3.Lerp(l_InitSetPos, l_SetPos, l_DistanceRate);
            l_DistanceRate += Time.deltaTime / BUILD_SPEED;
            yield return null;
        }

        m_VerticalPossible = Enumerable.Repeat(0, m_VerticalPossible.Length).ToArray();
        m_HorizontalPossible = Enumerable.Repeat(0, m_VerticalPossible.Length).ToArray();
        m_HalfWallVerticalPossible = Enumerable.Repeat(0, m_HalfWallVerticalPossible.Length).ToArray();
        m_HalfWallHorizontalPossible = Enumerable.Repeat(0, m_HalfWallHorizontalPossible.Length).ToArray();
        m_TopRightPossible = Enumerable.Repeat(0, m_TopRightPossible.Length).ToArray();
        m_TopLeftPossible = Enumerable.Repeat(0, m_TopLeftPossible.Length).ToArray();
        m_BottomRightPossible = Enumerable.Repeat(0, m_BottomRightPossible.Length).ToArray();
        m_BottomLeftPossible = Enumerable.Repeat(0, m_BottomLeftPossible.Length).ToArray();
        m_StateManager.UpdateState(StateManager.TurnState.End);
        yield break;
    }

    /// <summary>
    /// 一マス壁が配置可能状態を切り替える
    /// </summary>
    /// <param name="l_Flg"></param>
    public void ChangeCanHalfWallSetFlg(bool l_Flg)
    {
        m_CanHalfWallSetFlg = l_Flg;

        m_RotateButton.SetActive(l_Flg);
        m_BuildButton.SetActive(l_Flg);
    }

    /// <summary>
    /// 1マス壁の配置ができるかをチェックする
    /// </summary>
    public void HalfWallCheck()
    {
        if (m_WallDirectionFlag)
        {
            ViolationHalfWall(m_HalfWallVerticalPossible);
        }
        else
        {
            ViolationHalfWall(m_HalfWallHorizontalPossible);
        }
    }

    /// <summary>
    /// 壁設置
    /// </summary>
    /// <returns>設置完了ならtrue返す</returns>
    public bool HalfWallSet()
    {
        if (m_WallSetViolationFlg) { return false; }

        if (m_WallDirectionFlag)
        {
            VerticalHalfWallSet();
        }
        else
        {
            HorizontalHalfWallSet();
        }
        /*m_BringWall.SpendingWall();*/
        return true;
    }

    /// <summary>
    /// 縦に1マス壁が置けるindexを配列に格納
    /// </summary>
    public void HelfWallVerticalCheck()
    {
        m_StageInfo = m_BoardManager.ReturnStageInfo();

        for (int x = 0; x < m_StageInfo.GetLength(0); x++)
        {
            for (int y = 0; y < m_StageInfo.GetLength(1) - 1; y++)
            {
                if ((m_StageInfo[x, y] & Bit.RIGHT) != 0) { continue; }

                if (!m_DfsSystem.IFStageMakeHalfWallVersion(new Grid(x, y), VERTICAL)) { continue; }

                SetVisitedHalfWallVersion(new Grid(x, y), m_HalfWallVerticalPossible);
            }
        }
    }

    /// <summary>
    /// 横に1マス壁が置けるindexを配列に格納
    /// </summary>
    public void HelfWallHorizontalCheck()
    {
        m_StageInfo = m_BoardManager.ReturnStageInfo();

        for (int x = 0; x < m_StageInfo.GetLength(0) - 1; x++)
        {
            for (int y = 0; y < m_StageInfo.GetLength(1); y++)
            {
                if ((m_StageInfo[x, y] & Bit.BOTTOM) != 0) { continue; }

                if (!m_DfsSystem.IFStageMakeHalfWallVersion(new Grid(x, y), HORIZONTAl)) { continue; }

                SetVisitedHalfWallVersion(new Grid(x, y), m_HalfWallHorizontalPossible);
            }
        }
    }

    /// <summary>
    /// 縦の1マス壁の配置
    /// </summary>
    private void VerticalHalfWallSet()
    {
        StartCoroutine(DisplayWallSet());
        int l_UpdateBit;

        l_UpdateBit = Bit.RIGHT | m_StageInfo[m_Index.X, m_Index.Y];
        m_BoardManager.GridUpdate(m_Index, l_UpdateBit);

        ChangeCanHalfWallSetFlg(false);
    }

    /// <summary>
    /// 横の1マス壁の配置
    /// </summary>
    private void HorizontalHalfWallSet()
    {
        StartCoroutine(DisplayWallSet());
        int l_UpdateBit;

        l_UpdateBit = Bit.BOTTOM | m_StageInfo[m_Index.X, m_Index.Y];
        m_BoardManager.GridUpdate(m_Index, l_UpdateBit);

        ChangeCanHalfWallSetFlg(false);
    }

    /// <summary>
    ///壁を配置の可否によって色変え、配置可能にさせる
    /// </summary>
    /// <param name="CheckArray">配置の可否を格納した配列</param>
    private void ViolationHalfWall(int[] CheckArray)
    {
        int CheckIndex = ToIndexHalfWallVersion(m_Index);
        
        if (CheckArray[CheckIndex] != -1)
        {
            m_WallSetViolationFlg = true;
            m_DisplayManager.SetWallColor(m_HalfWall, false);
            return;
        }
        m_WallSetViolationFlg = false;
        m_DisplayManager.SetWallColor(m_HalfWall, true);
    }

    // 壁設置可能位置の設定を行う
    private void SetVisitedHalfWallVersion(Grid possibleGrid, int[] PossibleArray)
    {
        int l_PossibleIndex = ToIndexHalfWallVersion(possibleGrid);
        PossibleArray[l_PossibleIndex] = -1;
    }

    // Cellを1次元配列のインデックスに変換
    private int ToIndexHalfWallVersion(Grid grid)
    {
        return grid.X + m_StageInfo.GetLength(0) * grid.Y;
    }

    // 1次元配列のインデックスをセルに変換
    private Grid ToCellHalfWallVersion(int index)
    {
        return new Grid(index % (m_StageInfo.GetLength(0) + 1), index / (m_StageInfo.GetLength(0) + 1));
    }

    /// <summary>
    /// マウスの示している個所から参照したいindexを取得
    /// </summary>
    public void HalfWallIndexMake()
    {
        m_TouchPos = m_TouchManager.TouchPosition();

        float l_XPos = m_TouchPos[0];
        float l_ZPos = m_TouchPos[1];

        for (int i = 0; i < m_StageInfo.GetLength(0); i++)
        {
            if (l_ZPos > m_CheckXPos)
            {
                m_Index.X = i;
                TestDisplayWall(PlayChange);
                break;
            }
            m_CheckXPos -= AdjustmentValue;
        }

        for (int i = 0; i < m_StageInfo.GetLength(1); i++)
        {
            m_CheckYPos += AdjustmentValue;
            if (l_XPos < m_CheckYPos)
            {
                m_Index.Y = i;
                m_CheckYPos = -m_CheckPos;
                break;
            }
        }

        if (!m_WallDirectionFlag && m_Index.X == m_StageInfo.GetLength(0) - 1)
        {
            m_Index.X--;
        }
        else if (m_WallDirectionFlag && m_Index.Y == m_StageInfo.GetLength(1) - 1)
        {
            m_Index.Y--;
        }

        MoveWall();
    }

    /// <summary>
    /// 壁が配置可能状態を切り替える
    /// </summary>
    public void ChangeCanLShapedWallSetFlg(bool l_Flg)
    {
        m_CanLShapedWallSetFlg = l_Flg;

        m_RotateButton.SetActive(l_Flg);
        m_BuildButton.SetActive(l_Flg);
    }

    /// <summary>
    /// Ｌ字型壁の向き変更
    /// </summary>
    public void LShapedWallRotation()
    {
        m_LShapedWallDirection++;
        if(m_LShapedWallDirection >= 4)
        {
            m_LShapedWallDirection = 0;
        }
        m_LShapedWall.transform.Rotate(new Vector3(0f, m_LShapedWall.transform.rotation.y + 90f, 0f));
    }

    /// <summary>
    /// L字型の壁が置けるindexを配列に格納
    /// </summary>
    public void LShapedWallPossibleCheck()
    {
        m_StageInfo = m_BoardManager.ReturnStageInfo();

        int[] l_PossibleList = new int[64];
        int X = -1;
        int Y = -1;
        for (int a = 0; a < 4; a++)
        {
            switch (a)
            {
                case 0:
                    l_PossibleList = m_TopLeftPossible;
                    break;
                case 1:
                    l_PossibleList = m_TopRightPossible;
                    break;
                case 2:
                    l_PossibleList = m_BottomRightPossible;
                    break;
                case 3:
                    l_PossibleList = m_BottomLeftPossible;
                    break;
            }

            for (int x = 0; x < m_StageInfo.GetLength(0) - 1; x++)
            {
                for (int y = 0; y < m_StageInfo.GetLength(1) - 1; y++)
                {
                    switch (a)
                    {
                        case 0:
                            if (((m_StageInfo[x, y] | m_StageInfo[x, y + 1]) & Bit.BOTTOM) != 0 || CrossCheck(new Grid(x, y))) { continue; }
                            if (x == 0 || y == m_StageInfo.GetLength(1) - 2) { continue; }
                            X = x - 1;
                            Y = y + 1;
                            if (((m_StageInfo[X, Y] | m_StageInfo[X + 1, Y]) & Bit.RIGHT) != 0 || CrossCheck(new Grid(X, Y))) { continue; }
                            break;
                        case 1:
                            if (((m_StageInfo[x, y] | m_StageInfo[x + 1, y]) & Bit.RIGHT) != 0 || CrossCheck(new Grid(x, y))) { continue; }
                            if (x == m_StageInfo.GetLength(0) - 2 || y == m_StageInfo.GetLength(1) - 2) { continue; }
                            X = x + 1;
                            Y = y + 1;
                            if (((m_StageInfo[X, Y] | m_StageInfo[X, Y + 1]) & Bit.BOTTOM) != 0 || CrossCheck(new Grid(X, Y))) { continue; }
                            break;
                        case 2:
                            if (((m_StageInfo[x, y] | m_StageInfo[x, y + 1]) & Bit.BOTTOM) != 0 || CrossCheck(new Grid(x, y))) { continue; }
                            if (x == m_StageInfo.GetLength(0) - 2 || y == 0) { continue; }
                            X = x + 1;
                            Y = y - 1;
                            if (((m_StageInfo[X, Y] | m_StageInfo[X + 1, Y]) & Bit.RIGHT) != 0 || CrossCheck(new Grid(X, Y))) { continue; }
                            break;
                        case 3:
                            if (((m_StageInfo[x, y] | m_StageInfo[x + 1, y]) & Bit.RIGHT) != 0 || CrossCheck(new Grid(x, y))) { continue; }
                            if (x == 0 || y == 0) { continue; }
                            X = x - 1;
                            Y = y - 1;
                            if (((m_StageInfo[X, Y] | m_StageInfo[X, Y + 1]) & Bit.BOTTOM) != 0 || CrossCheck(new Grid(X, Y))) { continue; }
                            break;
                    }

                    if (a == 1 || a == 3)
                    {
                        if (!m_DfsSystem.IfStageMakeLShapedWallVersion(new Grid(x, y), new Grid(X, Y),VERTICAL)) { continue; }
                    }
                    else
                    {
                        if (!m_DfsSystem.IfStageMakeLShapedWallVersion(new Grid(x, y), new Grid(X, Y),HORIZONTAl)) { continue; }
                    }
                    SetVisitedLShapedWallVersion(new Grid(x, y), l_PossibleList);
                }
            }
        }
    }

    // 壁設置可能位置の設定を行う
    private void SetVisitedLShapedWallVersion(Grid possibleGrid, int[] PossibleArray)
    {
        int l_PossibleIndex = ToIndex(possibleGrid);
        PossibleArray[l_PossibleIndex] = -1;
    }

    /// <summary>
    /// 壁の配置ができるかをチェックする
    /// </summary>
    public void LShapedWallCheck()
    {
        int[] l_PossibleList = new int[64];

        switch (m_LShapedWallDirection)
        {
            case 0:
                l_PossibleList = m_TopLeftPossible;
                break;
            case 1:
                l_PossibleList = m_TopRightPossible;
                break;
            case 2:
                l_PossibleList = m_BottomRightPossible;
                break;
            case 3:
                l_PossibleList = m_BottomLeftPossible;
                break;
        }

        ViolationLShapedWall(l_PossibleList);
    }

    /// <summary>
    /// L字型の壁を配置の可否によって色変え、配置可能にさせる
    /// </summary>
    /// <param name="CheckArray">配置の可否を格納した配列</param>
    private void ViolationLShapedWall(int[] CheckArray)
    {
        int CheckIndex = ToIndex(m_Index);
        if (CheckArray[CheckIndex] != -1)
        {
            m_WallSetViolationFlg = true;
            m_DisplayManager.SetLShapedWallColor(m_LShapedWall, false);
            return;
        }

        m_WallSetViolationFlg = false;
        m_DisplayManager.SetLShapedWallColor(m_LShapedWall, true);
    }

    /// <summary>
    /// 縦の壁の配置
    /// </summary>
    private void LShapedVerticalSet()
    {
        int l_UpdateBit;

        l_UpdateBit = Bit.RIGHT | Bit.CROSS | m_StageInfo[m_Index.X, m_Index.Y];
        m_BoardManager.GridUpdate(m_Index, l_UpdateBit);

        Grid l_Index = new Grid(m_Index.X + 1, m_Index.Y);

        l_UpdateBit = Bit.RIGHT | m_StageInfo[l_Index.X, l_Index.Y];
        m_BoardManager.GridUpdate(l_Index, l_UpdateBit);

        ChangeCanWallSetFlg(false);
    }

    /// <summary>
    /// 横の壁の配置
    /// </summary>
    private void LShapedHorizontalSet()
    {
        int l_UpdateBit;

        l_UpdateBit = Bit.BOTTOM | Bit.CROSS | m_StageInfo[m_Index.X, m_Index.Y];
        m_BoardManager.GridUpdate(m_Index, l_UpdateBit);

        Grid l_Index = new Grid(m_Index.X, m_Index.Y + 1);

        l_UpdateBit = Bit.BOTTOM | m_StageInfo[l_Index.X, l_Index.Y];
        m_BoardManager.GridUpdate(l_Index, l_UpdateBit);

        ChangeCanWallSetFlg(false);
    }

    /// <summary>
    /// L字型の壁設置
    /// </summary>
    /// <returns>設置完了ならtrue返す</returns>
    public bool LShapedWallSet()
    {
        if (m_WallSetViolationFlg) { return false; }

        if (m_LShapedWallDirection == 1 || m_LShapedWallDirection == 3)
        {
            LShapedVerticalSet();
            m_Index = m_SecondIndex;
            LShapedHorizontalSet();
        }
        else
        {
            LShapedHorizontalSet();
            m_Index = m_SecondIndex;
            LShapedVerticalSet();
        }
        StartCoroutine(DisplayWallSet());
        /*m_BringWall.SpendingWall();*/

        ChangeCanLShapedWallSetFlg(false);
        return true;
    }

    /// <summary>
    /// マウスの示している個所から参照したいindexを取得
    /// </summary>
    public void LShapedWallIndexMake()
    {
        m_TouchPos = m_TouchManager.TouchPosition();

        float l_XPos = m_TouchPos[0];
        float l_ZPos = m_TouchPos[1];

        for (int i = 0; i < m_StageInfo.GetLength(0) - 1; i++)
        {
            m_CheckXPos -= AdjustmentValue;
            if (l_ZPos > m_CheckXPos)
            {
                m_Index.X = i;
                TestDisplayWall(PlayChange);
                break;
            }      
        }

        for (int i = 0; i < m_StageInfo.GetLength(1) - 1; i++)
        {
            m_CheckYPos += AdjustmentValue;
            if (l_XPos < m_CheckYPos)
            {
                m_Index.Y = i;
                m_CheckYPos = -m_CheckPos;
                break;
            }
        }

        switch (m_LShapedWallDirection)
        {
            case 0:
                if (m_Index.X == 0) { m_Index.X++; }
                if (m_Index.Y == m_StageInfo.GetLength(1) - 2) { m_Index.Y--; }
                m_SecondIndex.X = m_Index.X - 1;
                m_SecondIndex.Y = m_Index.Y + 1;
                break;
            case 1:
                if (m_Index.X == m_StageInfo.GetLength(0) - 2) { m_Index.X--; }
                if (m_Index.Y == m_StageInfo.GetLength(1) - 2) { m_Index.Y--; }
                m_SecondIndex.X = m_Index.X + 1;
                m_SecondIndex.Y = m_Index.Y + 1;
                break;
            case 2:
                if (m_Index.X == m_StageInfo.GetLength(0) - 2) { m_Index.X--; }
                if (m_Index.Y == 0) { m_Index.Y++; }
                m_SecondIndex.X = m_Index.X + 1;
                m_SecondIndex.Y = m_Index.Y - 1;
                break;
            case 3:
                if (m_Index.X == 0) { m_Index.X++; }
                if (m_Index.Y == 0) { m_Index.Y++; }
                m_SecondIndex.X = m_Index.X - 1;
                m_SecondIndex.Y = m_Index.Y - 1;
                break;
        }

        MoveWall();
    }
}