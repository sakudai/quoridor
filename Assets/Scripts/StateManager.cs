﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    public enum GameState
    {
        /// <summary>
        /// スキル選択
        /// </summary>
        SkillSelect,

        /// <summary>
        /// スタート準備
        /// </summary>
        Ready,

        /// <summary>
        /// ゲームスタート
        /// </summary>
        Start,

        /// <summary>
        /// 0番のターン中
        /// </summary>
        Piece0,

        /// <summary>
        /// 1番のターン中
        /// </summary>
        Piece1,

        /// <summary>
        /// 2番のターン中
        /// </summary>
        Piece2,

        /// <summary>
        /// 3番のターン中
        /// </summary>
        Piece3,

        /// <summary>
        /// ポーズ中
        /// </summary>
        Pause,

        /// <summary>
        /// ゲーム終了
        /// </summary>
        End,

        /// <summary>
        /// 待機中
        /// </summary>
        Stay,
    }

    public enum TurnState
    {
        /// <summary>
        /// 待機中
        /// </summary>
        Stay,

        /// <summary>
        /// プレイヤーの選択待ち
        /// </summary>
        Wait,

        /// <summary>
        /// 壁選択中
        /// </summary>
        Wall,

        /// <summary>
        /// 駒選択中
        /// </summary>
        Piece,

        /// <summary>
        /// カード選択中
        /// </summary>
        Card,

        /// <summary>
        /// スキル使用中
        /// </summary>
        Skill,

        /// <summary>
        /// 確認中
        /// </summary>
        Check,

        /// <summary>
        /// ターン終了
        /// </summary>
        End,
    }

    public enum SkillState
    {
        /// <summary>
        /// 待機中
        /// </summary>
        Stay,

        /// <summary>
        /// 2回移動
        /// </summary>
        TwoTimesMove,

        /// <summary>
        /// ななめ移動
        /// </summary>
        ObliqueMove,

        /// <summary>
        /// 相手をななめに吹き飛ばす
        /// </summary>
        ObliqueBlowAway,

        /// <summary>
        /// 1マス壁を造る
        /// </summary>
        HalfWallBuild,

        /// <summary>
        /// L字型の壁を作る
        /// </summary>
        LShapedWallBuild,
    }

    [SerializeField]
    private GameState m_CurrentGameState = new GameState();

    [SerializeField]
    private TurnState m_CurrentTurnState = new TurnState();

    [SerializeField]
    private SkillState m_CurrentSkillState = new SkillState();

    /// <summary>
    /// ゲーム状態を変える
    /// </summary>
    /// <param name="l_gamestate">変更後の状態</param>
    public void UpdateState(GameState l_gamestate)
    {
        m_CurrentGameState = l_gamestate;
    }

    /// <summary>
    /// ターン状態を変える
    /// </summary>
    /// <param name="l_turnstate">変更後の状態</param>
    public void UpdateState(TurnState l_turnstate)
    {
        m_CurrentTurnState = l_turnstate;
    }

    /// <summary>
    /// スキル状態を変える
    /// </summary>
    /// <param name="l_turnstate">変更後の状態</param>
    public void UpdateState(SkillState l_turnstate)
    {
        m_CurrentSkillState = l_turnstate;
    }

    /// <summary>
    /// ゲーム状態チェック
    /// </summary>
    /// <param name="l_gamestate">チェックする状態</param>
    public bool CheckState(GameState l_gamestate)
    {
        return l_gamestate == m_CurrentGameState;
    }

    /// <summary>
    /// ターン状態チェック
    /// </summary>
    /// <param name="l_turnstate">チェックする状態</param>
    public bool CheckState(TurnState l_turnstate)
    {
        return l_turnstate == m_CurrentTurnState;
    }

    /// <summary>
    /// スキル状態チェック
    /// </summary>
    /// <param name="l_turnstate">チェックする状態</param>
    public bool CheckState(SkillState l_turnstate)
    {
        return l_turnstate == m_CurrentSkillState;
    }
}
