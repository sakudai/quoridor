﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfWallBuildSkill : MonoBehaviour, ISkill
{
    private StateManager m_StateManager;

    private WallManager m_WallManager; 

    private void Start()
    {
        m_StateManager = GameObject.FindGameObjectWithTag("System").GetComponent<StateManager>();
        m_WallManager = GameObject.FindGameObjectWithTag("System").GetComponent<WallManager>();
    }

    public void Use(int l_TurnNum)
    {
        m_WallManager.HelfWallHorizontalCheck();
        m_WallManager.HelfWallVerticalCheck();
        m_WallManager.ChangeCanHalfWallSetFlg(true);
        m_StateManager.UpdateState(StateManager.TurnState.Skill);
        m_StateManager.UpdateState(StateManager.SkillState.HalfWallBuild);
    }
}
