﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    private CreateStage m_CreateStage;

    private BoardManager m_Boardmanager;

    private PieceManager m_PieceManager;

    private GoalManager m_GoalManager;

    private const int STAGE_SIZE = 13;

    private int[,] DefaultStage ={
        { 0,0,0,0,0,0,0,0,0,},
        { 0,0,0,0,0,0,0,0,0,},
        { 0,0,0,0,0,0,0,0,0,},
        { 0,0,0,0,0,0,0,0,0,},
        { 0,0,0,0,0,0,0,0,0,},
        { 0,0,0,0,0,0,0,0,0,},
        { 0,0,0,0,0,0,0,0,0,},
        { 0,0,0,0,0,0,0,0,0,},
        { 0,0,0,0,0,0,0,0,0,},
    };

    private void Awake()
    {
        m_CreateStage = GetComponent<CreateStage>();
        m_Boardmanager = GetComponent<BoardManager>();
        m_PieceManager = GetComponent<PieceManager>();
        m_GoalManager = GetComponent<GoalManager>();
    }

    /// <summary>
    /// ステージの初期配置処理
    /// </summary>
    /// <param name="l_PieceCount">駒の総数</param>
    public void StageInitialize(int l_PieceCount)
    {
        m_CreateStage.StageCreate(DefaultStage);

        m_Boardmanager.InitializeStage(DefaultStage);

        m_CreateStage.InitializePiecePos(l_PieceCount, DefaultStage);
        m_PieceManager.AddListPieceIndex(l_PieceCount);
        m_GoalManager.AddListGoalIndex(l_PieceCount);
    }
}
