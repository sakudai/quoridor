﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class SkillManager : MonoBehaviour
{
    private const int MAX_MOVE_COUNT = 2;

    private StateManager m_StateManager;

    private CheckManager m_CheckManager;

    private int m_PieceMoveCount;

    /*[SerializeField]
    private GameObject m_Piece0SkillManager;

    [SerializeField]
    private GameObject m_Piece1SkillManager;

    [SerializeField]
    private GameObject m_Piece2SkillManager;

    [SerializeField]
    private GameObject m_Piece3SkillManager;

    private List<GameObject> m_SkillManagerObjList;*/

    [SerializeField]
    private GameObject m_SkillObj;

    [SerializeField]
    private GameObject m_SkillObj1;

    [SerializeField]
    private GameObject m_SkillObj2;

    [SerializeField]
    private GameObject m_SkillObj3;

    [SerializeField]
    private GameObject m_SkillObj4;

    private List<GameObject> m_SkillList;

    private List<GameObject> m_UseSkillList = new List<GameObject>();

    private bool[] m_UsedFlgList;

    [SerializeField]
    private ToggleGroup m_ToggleGroup;

    private int m_CurrentSkillSelectNum = 0;

    private int m_MaxPieceNum;

    [SerializeField]
    private GameObject m_SkillSelectCanvas;

    [SerializeField]
    private GameObject m_SkillButton;

    private void Start()
    {
        m_StateManager = GetComponent<StateManager>();
        m_CheckManager = GetComponent<CheckManager>();

        /*m_SkillManagerObjList =  new List<GameObject>() { m_Piece0SkillManager, m_Piece1SkillManager, m_Piece2SkillManager, m_Piece3SkillManager };*/
        m_SkillList = new List<GameObject>() { m_SkillObj, m_SkillObj1, m_SkillObj2, m_SkillObj3, m_SkillObj4 };
        m_UsedFlgList = new bool[m_MaxPieceNum];
    }

    /*/// <summary>
    /// スキルを管理オブジェクトの子供にする
    /// </summary>
    /// <param name="l_TurnNum">スキルを持つ駒の番号</param>
    /// <param name="l_SkillCardObj">スキルカードのオブジェクト</param>
    public void SkillSet(int l_TurnNum, GameObject l_SkillCardObj)
    {
        l_SkillCardObj.transform.parent = m_SkillManagerObjList[l_TurnNum].transform;
    }*/

    /// <summary>
    /// 使用するスキルをリストに追加する
    /// </summary>
    /// <param name="l_SkillNum"></param>
    private void SkillSet(int l_SkillNum)
    {
        m_UseSkillList.Add(m_SkillList[l_SkillNum]);
        m_CurrentSkillSelectNum++;

        if(m_CurrentSkillSelectNum == m_MaxPieceNum)
        {
            m_SkillSelectCanvas.SetActive(false);
            m_StateManager.UpdateState(StateManager.GameState.Ready);
        }
    }

    /// <summary>
    /// 人数を受け取る
    /// </summary>
    /// <param name="l_MaxNum"></param>
    public void UpdateMaxPieceNum(int l_MaxNum)
    {
        m_MaxPieceNum = l_MaxNum;
    }

    /*/// <summary>
    /// スキルカードを表示する
    /// </summary>
    /// <param name="l_TurnNum">スキルを表示する駒の番号</param>
    public void DisplaySkillCard(int l_TurnNum)
    {
        m_SkillManagerObjList[l_TurnNum].SetActive(true);
    }

    /// <summary>
    /// スキルカードを非表示にする
    /// </summary>
    /// <param name="l_TurnNum">スキルを非表示にする駒の番号</param>
    public void RemoveSkillCard(int l_TurnNum)
    {
        m_SkillManagerObjList[l_TurnNum].SetActive(false);
    }

    /// <summary>
    /// クリックしたオブジェクトがカードなら確認処理挟んで使用
    /// </summary>
    /// <returns></returns>
    public IEnumerator GetClickObj(int l_TurnNum)
    {
        GameObject l_ClickedGameObject = null;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();

        if (Physics.Raycast(ray, out hit))
        {
            l_ClickedGameObject = hit.collider.gameObject;
        }

        if(l_ClickedGameObject == null)
        {
            yield break; 
        }

        ISkill l_ISkill = l_ClickedGameObject.GetComponent<ISkill>();

        if(l_ISkill == null)
        {
            yield break;
        }

        bool l_CheckFlg = false;

        //チェック入る
        yield return StartCoroutine(m_CheckManager.WaitFixed(b => l_CheckFlg = b));

        //中止ボタンが押されたなら中止
        if (l_CheckFlg == false)
        {
            m_StateManager.UpdateState(StateManager.TurnState.Card);
            yield break;
        }

        Initialize();
        l_ISkill.Use(l_TurnNum);

        l_ClickedGameObject.GetComponent<Renderer>().enabled = false;
        l_ClickedGameObject.GetComponent<Collider>().enabled = false;
        yield break;
    }*/

    /// <summary>
    /// スキルの使用メソッドを呼ぶ
    /// </summary>
    /// <returns></returns>
    public IEnumerator CallingUse(int l_TurnNum)
    {
        bool l_CheckFlg = false;

        //チェック入る
        yield return StartCoroutine(m_CheckManager.WaitFixed(b => l_CheckFlg = b));

        //中止ボタンが押されたなら中止
        if (l_CheckFlg == false)
        {
            m_StateManager.UpdateState(StateManager.TurnState.Wait);
            yield break;
        }

        Initialize();
        m_UseSkillList[l_TurnNum].GetComponent<ISkill>().Use(l_TurnNum);
        m_UsedFlgList[l_TurnNum] = true;
    }


    /// <summary>
    /// スキルボタンの表示、非表示
    /// </summary>
    /// <param name="l_TurnNum">ターンの番号</param>
    public void ButtonDisplayCheck(int l_TurnNum)
    {
        //使用済みならボタン非表示
        m_SkillButton.SetActive(!m_UsedFlgList[l_TurnNum]);
    }

    /// <summary>
    /// 選択されたスキルの番号を判別する
    /// </summary>
    public void GetSkillNum()
    {
        if (!m_ToggleGroup.AnyTogglesOn()) { return; }

        int l_SkillNum = -1;
        //for (int i = 0; i < m_ToggleGroup.gameObject.transform.childCount; i++)
        //{
        //    Debug.Log(m_ToggleGroup.ActiveToggles().FirstOrDefault());
        //    if (m_ToggleGroup.gameObject.transform.GetChild(i).GetComponent<Toggle>().isOn)
        //    {
        //        l_SkillNum = i;
        //        break;
        //    }
        //}
        int i = -1;
        foreach(Transform child in m_ToggleGroup.transform)
        {
            i++;
            if (child.GetComponent<Toggle>().isOn)
            {
                l_SkillNum = i;
                child.GetComponent<Toggle>().isOn = false;
                break;
            }
        }

        SkillSet(l_SkillNum);

        Debug.Log(l_SkillNum);
    }

    /// <summary>
    /// 初期化
    /// </summary>
    private void Initialize()
    {
        m_PieceMoveCount = 0;
    }

    /// <summary>
    /// 2回移動スキル使用時のカウントチェック
    /// </summary>
    /// <returns></returns>
    public bool CheckMoveCount()
    {
        if(m_PieceMoveCount == MAX_MOVE_COUNT - 1){ return true; }
        m_PieceMoveCount++;
        return false;
    }
}