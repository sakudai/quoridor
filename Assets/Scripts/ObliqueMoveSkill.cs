﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObliqueMoveSkill : MonoBehaviour, ISkill
{
    private StateManager m_StateManager;

    private PieceManager m_PieceManager;

    private void Start()
    {
        m_StateManager = GameObject.FindGameObjectWithTag("System").GetComponent<StateManager>();
        m_PieceManager = GameObject.FindGameObjectWithTag("System").GetComponent<PieceManager>();
    }

    public void Use(int l_TurnNum)
    {
        //移動可能マスがないなら選択前に戻す
        if (m_PieceManager.CallingObliqueMovableCheck(l_TurnNum))
        {
            m_StateManager.UpdateState(StateManager.TurnState.Wait);
            return;
        }

        m_StateManager.UpdateState(StateManager.TurnState.Skill);
        m_StateManager.UpdateState(StateManager.SkillState.ObliqueMove);

    }
}
